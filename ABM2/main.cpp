// include the basic windows header files and the Direct3D header file
#ifdef WIN32
#include <windows.h>
#include <windowsx.h>
#endif

#include "Engine/engine.h"
#include "Engine/game.h"
#include "Engine/SpriteManager.h"
#include "Playground.h"
#include "SchemeReader.h"

// define the screen resolution and keyboard macros
#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

SpriteData defaultSprites[] = {
	{SPR_FIELD, "Data/FIELD0.png", SCREEN_WIDTH, SCREEN_HEIGHT},
	{SPR_TILE, "Data/Tiles0.png", FIELD_UNIT_WIDTH, FIELD_UNIT_HEIGHT},
	{SPR_NONE, NULL, 0, 0}
};

#ifdef WIN32
HINSTANCE hInstance;
HWND hWnd;

// the WindowProc function prototype
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

class CPongTask : public ITask
{
public:
	float p1pos, p2pos;
	float paddleWidth, paddleHeight;
	float ballX, ballY, ballVX, ballVY;
	float ballSize;

	bool Start()
		{
/*			InitRandomNumbers();

			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0.0f, 1.0f, 1.0f, 0.0f, -1.0f, 1.0f);

			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glShadeModel(GL_SMOOTH);
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

			paddleWidth=0.1f; paddleHeight=0.01f;
			p1pos=p2pos=0.5f;
			ballX=0.5f; ballY=0.5f;
			ballVX=(float)(rand()%20-10)/20;
			ballVY=(float)(rand()%20-10)/20;
			ballSize=0.01f;
*/
			return true;
		}
	void Update()
		{
			if(CInputTask::keyDown(DIK_ESCAPE))CKernel::GetSingleton().KillAllTasks();
			/*
			glClear(GL_COLOR_BUFFER_BIT);
			

			glBegin(GL_QUADS);
			{
				glColor4f(1.0f,1.0f,1.0f,1.0f);
				
				//draw the ball
				glVertex2f(ballX-ballSize, ballY-ballSize);
				glVertex2f(ballX+ballSize, ballY-ballSize);
				glVertex2f(ballX+ballSize, ballY+ballSize);
				glVertex2f(ballX-ballSize, ballY+ballSize);
				
				//paddles
				glVertex2f(p2pos-paddleWidth, 0);
				glVertex2f(p2pos+paddleWidth, 0);
				glVertex2f(p2pos+paddleWidth, paddleHeight);
				glVertex2f(p2pos-paddleWidth, paddleHeight);

				glVertex2f(p1pos-paddleWidth, 1-paddleHeight);
				glVertex2f(p1pos+paddleWidth, 1-paddleHeight);
				glVertex2f(p1pos+paddleWidth, 1);
				glVertex2f(p1pos-paddleWidth, 1);
			}
			glEnd();

			p1pos+=((float)CInputTask::dX)/200.0f;
			if(p1pos<paddleWidth)p1pos=paddleWidth;
			if(p1pos>1-paddleWidth)p1pos=1-paddleWidth;

			ballX+=ballVX*CGlobalTimer::dT; ballY+=ballVY*CGlobalTimer::dT;
			if(ballX<ballSize)ballVX=qAbs(ballVX);
			if(ballX>1-ballSize)ballVX=-qAbs(ballVX);
			if(ballY<ballSize+paddleHeight)
			{
				if((ballX>p2pos-paddleWidth)&&(ballX<p2pos+paddleWidth))
				{
					ballVY=qAbs(ballVY);
				}else{
					CKernel::GetSingleton().KillAllTasks();
				}
			}
			if(ballY>1-ballSize-paddleHeight)
			{
				if((ballX>p1pos-paddleWidth)&&(ballX<p1pos+paddleWidth))
				{
					ballVY=-qAbs(ballVY);
				}else{
					CKernel::GetSingleton().KillAllTasks();
				}
			}

			if(ballX>p2pos)p2pos+=0.1f*CGlobalTimer::dT;
			if(ballX<p2pos)p2pos-=0.1f*CGlobalTimer::dT;
			if(p2pos<paddleWidth)p2pos=paddleWidth;
			if(p2pos>1-paddleWidth)p2pos=1-paddleWidth;
*/
		}
	void Stop()
	{

	};
	AUTO_SIZE;
};

#endif // WIN32
void CApplication::Run(int argc, char *argv[])
{
	//open logfiles
	if(!CLog::Get().Init())return;

	//create a couple of singletons
	new CSettingsManager();
	CKernel *kernel = new CKernel();

	//parse the 'settings.eng' file
	CSettingsManager::GetSingleton().ParseFile("settings.esf");
	
	//parse command-line arguments
	//skip the first argument, which is always the program name
	if(argc>1)
		for(int i=1;i<argc;i++)
			CSettingsManager::GetSingleton().ParseSetting(std::string(argv[i]));
	
	videoTask = new CVideoUpdate(kernel);
	videoTask->priority=10000;
	kernel->AddTask(CMMPointer<ITask>(videoTask));

	inputTask = new CInputTask(kernel);
	inputTask->priority=20;
	kernel->AddTask(CMMPointer<ITask>(inputTask));

	soundTask = new CSoundTask(kernel);
	soundTask->priority=50;
	kernel->AddTask(CMMPointer<ITask>(soundTask));

	globalTimer=new CGlobalTimer(kernel);
	globalTimer->priority=10;
	kernel->AddTask(CMMPointer<ITask>(globalTimer));

	CSpriteManager *sm = new CSpriteManager(videoTask->getGraphics(), defaultSprites);
	kernel->setSpriteManager(sm);

	Playground game(kernel);
	game.priority=100;
	kernel->AddTask(CMMPointer<ITask>(&game));


	//set up the profiler with an output handler
	CProfileLogHandler profileLogHandler;
	CProfileSample::outputHandler=&profileLogHandler;

	//main game loop
	kernel->Execute();
	
	//clean up singletons
	delete kernel;
	delete CSettingsManager::GetSingletonPtr();
}

#ifdef WIN32

// the entry point for any Windows program
int WINAPI WinMain(HINSTANCE hInst,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
  WNDCLASS wc;


	// register window class
	if (!hPrevInstance)
	{
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wc.hInstance = hInstance;
		wc.lpfnWndProc = &WindowProc;
		wc.lpszClassName = L"MainRollBallWClass";
		wc.lpszMenuName = L"MainMenu";
		wc.style = CS_HREDRAW | CS_VREDRAW;

		if (!RegisterClass(&wc)) return 0;
	}
	hInstance = hInst;	

	// create main window
	hWnd = CreateWindowEx(0, L"MainRollBallWClass", L"Main Rolling Ball Window", WS_OVERLAPPEDWINDOW, 200, 240,
			SCREEN_WIDTH+8, SCREEN_HEIGHT+26, NULL, NULL, hInstance, NULL);

    ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

    // set up and initialize Direct3D
	new CApplication();
	CApplication::GetSingleton().Run(NULL, NULL);
	delete CApplication::GetSingletonPtr();

	//clean up any remaining unreleased objects
	IMMObject::CollectRemainingObjects(true);

	PostMessage(hWnd, WM_DESTROY, 0, 0);
  return 0;
}

// this is the main message handler for the program
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_DESTROY:
            {
                PostQuitMessage(0);
                return 0;
            } break;
    }

    return DefWindowProc (hWnd, message, wParam, lParam);
}
#else // WIN32
int main(int argc, char *argv[])
{
  new CApplication();
  CApplication::GetSingleton().Run(argc,argv);
  delete CApplication::GetSingletonPtr();

  //clean up any remaining unreleased objects
  IMMObject::CollectRemainingObjects(true);

  return 0;
}
#endif //WIN32

