// stdafx.cpp : source file that includes just the standard includes
// Bomberman_SchemeReader.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information
#define _CRT_SECURE_NO_DEPRECATE
#define min(a, b)  (((a) < (b)) ? (a) : (b)) 

#include "SchemeReader.h"
#include <fstream>

using namespace std;

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

int StrIndex(char* source, int start, int length, char* dest) {
	if (source == NULL || dest == NULL) return 0;
	int stringlength = strlen(source);
	int copylen = min(length, stringlength);
	if (start > stringlength) return 0;//stupid check
	if (start+length > strlen(source)) length = stringlength - start;
	strncpy(dest, source+start, length);
	dest[length]=0;
	return length;
}

SchemeReader::SchemeReader(char *Filename) {
	char ReadBuffer[256];
	int num=0, counter;
	char rowcontents[PLAYGROUND_NUMFIELD_X+1];
	int startx,starty,team;
	int BornWith, HasOverride, Override, Forbidden;

	ifstream SchemeFile (Filename, ifstream::in);
	while (!(SchemeFile.eof())) {
		SchemeFile.getline(ReadBuffer, 256);
		switch(ReadBuffer[0]) {
			case '-': {
				switch(ReadBuffer[1]) {
					case 'R': {
						//PlayField data
						sscanf(ReadBuffer, "-R,%d,%s", &num, rowcontents);
						for (counter=0; counter < strlen(rowcontents); counter++) {
							switch(rowcontents[counter]) {
								case '#': 
									PlayField[num*PLAYGROUND_NUMFIELD_X+counter] = SOLID;
									break;
								case ':':
									PlayField[num*PLAYGROUND_NUMFIELD_X+counter] = BRICK;
									break;
								case '.':
									PlayField[num*PLAYGROUND_NUMFIELD_X+counter] = BLANK;
							}
						}
						break;
					}
					case 'S': {
						//Player Starting position data
						sscanf(ReadBuffer, "-S,%d,%d,%d,%d", &num, &startx, &starty, &team);
						Player[num].startx=startx;
						Player[num].starty=starty;
						Player[num].team=team;
						break;
					}
					case 'P': {
						//Powerup data
						sscanf(ReadBuffer, "-P,%d,%d,%d,%d,%d", &num, &BornWith, &HasOverride, &Override, &Forbidden);
						Powerups[num].BornWith=BornWith;
						Powerups[num].HasOverride=HasOverride;
						Powerups[num].Override=Override;
						Powerups[num].Forbidden=Forbidden;
						break;
					}
					case 'N': {
						//Scheme Name
						StrIndex(ReadBuffer, 3,256, Name);
						break;
					}
					case 'B': {
						//Brick Density
						sscanf(ReadBuffer, "-B,%d", &BrickDensity);
						break;
					}
					case 'V': {
						//Version
						//ignore
						break;
					}
				}
				break;
			}
		}
	}
	SchemeFile.close();
}
