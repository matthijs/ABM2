/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Network.h"		

#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>

#include <ctype.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <errno.h>
#include <sys/types.h>


Socket::Socket(int sd)
{	
	int x = 1;
	s=sd;
	if(setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &x, sizeof(x)) == -1)
		perror("sockopt");
	this->type=AM_TCP;

	return ;
}

Socket::Socket(int type, int protocol)
{
	int x = 1;
	int t = SOCK_STREAM;

	if(type == AM_UDP)
		t  = SOCK_DGRAM;

	s=socket(AF_INET, t, protocol);
	if(setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &x, sizeof(x)) == -1)
		perror("sockopt");

	this->type=type;

	return ;
}

Socket::~Socket()
{
	shutdown(s, SHUT_RDWR);
	
	return ;
}

int Socket::Bind(int port)
{
	struct sockaddr_in myaddr;

	myaddr.sin_family=AF_INET;
	myaddr.sin_port=htons(port);
	myaddr.sin_addr.s_addr=htonl(INADDR_ANY);

	int res=bind(s, (struct sockaddr *)&myaddr, sizeof(myaddr));
	if (res ==-1)
		perror("bind returned -1"), exit(1);
	
	return res;
}

int Socket::Listen(int log)
{
	int res=listen(s, log);
	if (res ==-1)
		perror("listen returned -1"), exit(1);
	
	return res;
}

Socket* Socket::Accept(void)
{
	socklen_t l = sizeof(struct sockaddr);

	int res = accept(s, (struct sockaddr *)&peer, &l);
	if(res == -1)
		perror("accept returned -1"), exit(1);
	
	Socket *tmp = new Socket((int)res);

	return tmp;
}


int Socket::Connect()
{
	struct in_addr tmp;
	tmp.s_addr = peer.sin_addr.s_addr;
	
	printf(" con to %s %d\n", inet_ntoa(tmp), ntohs(peer.sin_port));
	int res; 
	res = connect(s, (struct sockaddr *)&peer, sizeof(peer));
	
	if (res ==-1)
		perror("connect returned -1");
	
	return res;
}
 



int Socket::Send(void *buffer, unsigned long size)
{
// 	printf("send buf=%s\n", (const char *)buffer);
	int res=send(s, (const char *)buffer, size, 0);
	
	if (res ==-1)
	{
		perror("send returned -1");
		if(errno == ENOTSOCK)		
		 	printf("ENOTSOCK\n");
		else
			printf("ERROR\n");
	}
	
	return res;
}

int Socket::Receive(void *buffer, unsigned long size)
{
	int res=recv(s, (char *)buffer, size, 0);
	if (res ==-1)
		perror("recv returned -1"), exit(1);

	return res;
}

int Socket::SendTo(void *buffer, unsigned long size)
{
	int res=sendto(s, (const char *)buffer, size, 0, (const struct sockaddr *)&peer, sizeof(peer));
	if (res ==-1)
		perror("sendto returned -1"), exit(1);

	return res;
}

int Socket::RecvFrom(void *buffer, unsigned long size)
{
	unsigned int l;
	int res=recvfrom(s, (char *)buffer, size, 0, (struct sockaddr *)&peer, &l);
	if (res ==-1)
		perror("recvfrom returned -1"), exit(1);

	return res;
}

void Socket::SetPeer(struct sockaddr_in data)
{
	memcpy(&peer, &data, sizeof(data));

	return ;
}


struct sockaddr_in Socket::GetPeer(void)
{
	return peer;
}

struct sockaddr_in Socket::CreatePeer(const char *address, int port)
{
	struct sockaddr_in youraddr;

	printf("Hostname address specified: %s\n", address);
	
	youraddr.sin_port=htons(port);
	youraddr.sin_family=AF_INET;
	youraddr.sin_addr.s_addr=0;

	if (isdigit(address[0]))
	{
		unsigned long addr;
		
		addr=inet_addr(address);
		
		memcpy((void *)&(youraddr.sin_addr), (const void *)&addr, sizeof(addr));
	}
	else
	{
		struct hostent *otherhost;
	
		otherhost = gethostbyname(address);

 		if(otherhost == NULL)
 			herror("otherhost");
		
 		if (otherhost != NULL)
			memcpy((void *)&(youraddr.sin_addr), (const void *)otherhost->h_addr_list[0], otherhost->h_length);
	}

	return youraddr;
}


int Socket::Select(unsigned long seconds, unsigned long microsecs)
{
	struct timeval tv;
	
	tv.tv_sec = seconds;
	tv.tv_usec = microsecs;
	
	fd_set rfds;
	FD_ZERO(&rfds);
	FD_SET(s, &rfds);
	
	int ret = select(s+1, &rfds, NULL, NULL, &tv);
	return ret;
}


int Socket::Select(unsigned long microsecs)
{
	struct timeval tv;
	
	tv.tv_sec = 0;
	tv.tv_usec = microsecs;
	
	fd_set rfds;
	FD_ZERO(&rfds);
	FD_SET(s, &rfds);
	
	int ret = select(s+1, &rfds, NULL, NULL, &tv);
	return ret;
}


Network::Network()
{
	;
}

Network::~Network()
{
	;
}

Socket * Network::getSocket(int type, int protocol)
{
	Socket *tmp=new Socket(type, protocol);

	return tmp;
}
