/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _Graphics
#define _Graphics


#define GL_GLEXT_PROTOTYPES 1

#ifdef _WIN32							// an eimaste se Windows
	#include <windows.h>
	#define _USE_MATH_DEFINES
#endif

/* #define GLX_GLXEXT_PROTOTYPES
 #include <GL/glx.h>
 #include <GL/glxext.h> */


extern char **argv;
extern int argc;

/********
group: Types
*/




struct colour
{
	float r;
	float g;
	float b;
	float a;
};

/*struct: particle */
struct particle
{
	float x;
	float y;
	float z;
	float size;
	colour col;
};




/*struct: vertex
This struct represents a 3D vertex

see also:
<COLOUR_RGBA>, <Graphics::renderVertexArray>, <Graphics::renderTriangle>*/
struct vertex
{
/*	field: 	u,v
	texture coordinates*/
	float u,v;
/*	field: col
	the vertex colour*/
	colour col;
/*	field: nx,ny,nz
	direction of the normal vector for the vertex */
	float nx,ny,nz;
/*fields: x,y,z
	3D coordinates for the vertex*/
	float x,y,z;

};


struct camera
{
	float camx, camy, camz;
	float lookx, looky, lookz;
	float upx, upy, upz;
};


/*struct: light
3D light type

see also:
<Graphics::setLight>, <Graphics::showLight>, <COLOUR_RGBA>*/
struct light
{
	/*field: type
	 the type of light. 	_type_ can be one of

			+ *AM_POINT_LIGHT*  specifies a point light. This type of light has a position within the scene but no single direction.
			They emit their light in all directions. Think of a light-bulb in your house ;)
			+ *AM_DIRECTIONAL_LIGHT* specifies a directional light. This type of light has a direction but not position.
			Think the sun here.
			+ *AM_SPOT_LIGHT* specifies a spot light. *NOTE* currently, spot light is partially implemented.	*/
	int type;
/*field: diffuse
	the diffuse RGBA intensity of the light */
	colour diffuse;
/*field: specular
	the specular RGBA intensity of the light*/
	colour specular;
/*field:ambient
	the ambient RGBA intensity of the light*/
	colour ambient;
/*fields: posx,posy,posz
	the position of the point light. *Only* for point lights*/
	float posx,posy,posz;

/*fields: dirx,diry,dirz
	the direction of the directional light. *Only* for directional lights */
	float dirx,diry,dirz;
/*field: range
	only for spot lights*/
	float range;
/*fields: falloff,theta,phi
	only for spot lights*/
	float falloff,theta,phi;
};


#define	AM_POINT_LIGHT					1
#define	AM_DIRECTIONAL_LIGHT	2
#define	AM_SPOT_LIGHT					3

#define AM_SOFTWARE_PARTICLE	4
#define AM_HARDWARE_PARTICLE	5

#define AM_CULL_FRONT		6
#define AM_CULL_BACK		7
#define AM_CULL_NONE		8

#define AM_PLAIN					9
#define AM_LINEAR					10
#define AM_ANISOTROPIC	11

#define AM_TRIANGLE_STRIP	12
#define AM_TRIANGLE_FAN		13
#define AM_TRIANGLES				14
#define AM_POINT_LIST				15
#define AM_LINE_LIST				16
#define AM_LINE_STRIP				17

#define AM_ALPHA			18
#define AM_RGBA				19

#define AM_UBYTE			20

#define AM_XYZ	21
#define AM_XZY	22
#define AM_ZXY	23
#define AM_ZYX	24
#define AM_YZX	25
#define AM_YXZ	26



/*
group: Macros
*/

/*function: COLOUR_RGBA

parameters:
 r,g,b,a - the Red, Green, Blue and Alpha colour components */
extern colour COLOUR_RGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a );




/*
group: Classes
*/

class Graphics;
class FontImp;

class Font
{
public:
	Font();
	~Font();
	
	Font(const char *name, int size, Graphics *g);
	
	void print( const wchar_t *str, int x1, int y1, int x2, int y2, colour c );
	void print( char *str, int x1, int y1, int x2, int y2, colour c );

private:
	FontImp *imp;
	/*	FTGLTextureFont *font;
		int fontSize;
		Graphics *g;*/
};


class Texture;
class GraphicsImp;


/* class: Graphics
 This class is the core class of the Amaltheia library. It handles the display and
 via its methods you can render 2D and 3D primitives.
*/
class Graphics
{

public:

/* group: Constructors*/
	/* constructor: Graphics
	
parameters:
		width - the desired width of the display's resolution
		height - the desired height of the display's resolution
		fullScreen - if *true* run in full screen, otherwise in a window
		bpp - the desired Bits Per Pixel for the display
		alpha - enables/disables alpha blending
		lighting - enables/disables lighting

see also:
			<createDisplay> 	*/
	Graphics( int width = 1024, int height = 768, int bpp = 16, bool fullScreen = false,
				 						bool alpha = false, bool lighting = false );
	~Graphics();

	
/* group: Methods */
	
	/*	Method: setWidth 
		Sets the width of the display
	
	parameters:
		w - the desired width
	
	see also:
		<getWidth>*/
	void setWidth( int w = 1024 );

/*	Method: getWidth
	Returns the width of the display

	returns:
		the current display's width in pixels
	
	see also:
		<setWidth>*/
	int getWidth();

/*	Method: setHeight
	Sets the height of the display

parameters:
	h - the desired height

see also:
	<getHeight>*/
	void setHeight( int h = 768 );
	
/*	Method: getHeight
	 Returns the height of the display

returns:
	the height of the display in pixels

see also:
	<setHeight>*/
	int getHeight();

	/*method: setBpp
	Sets the Bits per Pixel for the display

	parameters:
		bpp - bits per pixel

	see also:
		<getBpp> */
	void setBpp( int bpp = 16);

	/*method: getBpp
	Returns the current BPP of the display

	returns:
		bits per pixel

	see also:
		<setBpp>*/
	int getBpp();

	void enableVsync( bool b);
	bool isVsync();

	
	void enableFullScreen( bool b);
	bool isFullScreen();

	/*method: enableLighting
		Enables/disables lighting in the scene

	parameters:
		*true* to enable lighting or *false* to disable it

	see also:
		<isLighting>	*/
	void enableLighting( bool );

	/*method: isLighting
	Is lighting enabled?

	returns:
		*true* if lighting is enabled, *false* otherwise

see also:
		<enableLighting>*/
	bool isLighting();

	/*method: enableDepthBuffer
	Enables/disables the depth (Z) buffer

	parameters:
		*true* to enable the Z-buffer, *false* to disable it

	see also:
	<isDepthBuffer>	*/
	void enableDepthBuffer( bool );

	/*method: isDepthBuffer
	Is the depth buffer enabled?

	returns:
		*true* if the depth buffer is enabled, *false* otherwise

	see also:
		<enableDepthBuffer>	*/
	bool isDepthBuffer();

	/*method: enableAlpha
	Enables/disables alpha blending

	parameters:
		*true* to enable alpha blending, *false* to disable it

	see also:
		<isAlpha>	*/
	void enableAlpha( bool );

	/*method: isAlpha
	Is alpha blending enabled?

	returns:
		*true* is alpha blending is enabled, *false* otherwise

	see also:
		<enableAlpha>	*/
	bool isAlpha();


	void enableCursor( bool );
	bool isCursor();

	
	/* method: createDisplay
		Creates the display. You should call this method *once*.

	see also:
	<Graphics>*/
	int createDisplay();

	/*method: beginScene
	You must call this method before starting drawing/rendering primitives to the display.
	This method does some initializations e.g clears the color and depth buffers, etc...

	see also:
		<endScene>*/
	void beginScene();

	/*method: endScene
	You must call this method after you have finished rendering the current frame and you want
	to be visible on the screen. <beginScene> and <endScene> methods must be pair, so you must
	call <beginScene> once, before calling <endScene>.

	see also:
		<beginScene>*/
	void endScene();


	
	/*method: setWorld
	Sets the world tranformations matrices (translation, rotation, scale)

	parameters:
	transx - number of units to translate in the X axis
	transy - number of units to translate in the Y axis
	transz - number of units to translate in the Z axis
	rotx - degrees to rotate in the X axis
	roty - degrees to rotate in the Y axis
	rotz - degrees to rotate in the Z axis
	scalex - scale factor in the X axis (a factor of 1.0 performs no scale)
	scaley - scale factor in the Y axis
	scalez - scale factor in the Z axis	*/
	int setWorld( float transx, float transy, float transz,
					  float rotx, float roty, float rotz,
					  float scalex, float scaley, float scalez, int rotationOrder = AM_XYZ);


	/*
	int setWorld2( float transx, float transy, float transz,
					  float rotx, float roty, float rotz,
					  float scalex, float scaley, float scalez );

	*/
	
	
	/*method: setCamera
	Specifies the position and the direction of the camera.

	parameters:
		camx - the x coordinate of the camera position
		camy - the y coordinate of the camera position
		camz - the z coordinate of the camera position
		lookx - the x coordinate of the reference point
		looky - the y coordinate of the reference point
		lookz - the z coordinate of the reference point
		upx -  the x coordinate of the up vector
		upy -  the y coordinate of the up vector
		upz - the z coordinate of the up vector

	see also:
		<getCamera> */
	int setCamera( float camx, float camy, float camz,
						 float lookx, float looky, float lookz,
	               float upx, float upy, float upz );

/*method: getCamera
	Returns a <camera> struct containing the position and the direction of the camera

	returns:
		a <camera> struct

	see also:
		<setCamera>*/
	struct camera getCamera();
	

	/*method: setBackground
	 Sets the background colour of the display.

	parameters:
		c - the colour to set. Use the COLOUR_RGBA() macro to specify it

	see also:
		<getBackground>*/
	void setBackground( colour c );

	/* method: getBackground

	returns:
		the colour of the display background

	see also:
		<setBackground> */
	colour getBackground();

	/* method: setTexture
	Sets which will be the current texture to use.

	parameters:
		tex - a pointer to a <Texture> object

	see also:
	<getTexture>, <Texture> class*/
	void setTexture( Texture *tex );

	/*method: getTexture

	returns:
		a pointer to the current <Texture> object

	see also:
	<setTexture>, <Texture> class	*/
	Texture* getTexture();
	
	/*method: setCullingMode
	Specifies the culling mode to use, whether front or back facets will be culled.
	The orientation of front-facing polygons is clock-wise.

	parameters:
		mode - the desired culling mode

	valid values for _mode_ are

	+ *AM_CULL_BACK* for culling back facets
	+ *AM_CULL_FRONT* for culling front facets
	+ *AM_CULL_NONE* for culling none of the facets

	see also:
		<getCullingMode>	*/
	void setCullingMode(int mode);

	/*method: getCullingMode

	returns:
		The current culling mode

	see also:
	<setCullingMode>	*/
	int  getCullingMode();


	/*method:  renderTriangle
	Renders a triangle in the display

	parameters:
	v1, v2, v3 - the 3 vertices that specify the triangle

	see also:
		<vertex> struct, <renderVertexArray>	*/
	int renderTriangle(vertex v1, vertex v2, vertex v3 );

	/*method: renderVertexArray
	Renders a mesh of vertices

	parameters:
		array - an array containing the vertices
		numOfVertices - the number of vertices in the array
		type - how to connect the vertices

	
	valid values for _type_ are
	
	+ *AM_TRIANGLE_STRIP*  renders a connected group of triangles. One triangle is defined for each vertex
	presented after the first two vertices. For odd i, vertices i, i+1, and i+2 define triangle i.
	For even i, vertices i+1, i, and i+2 define triangle i. 'numOfVertices-2' triangles are rendered
	+ *AM_TRIANGLE_FAN* 	renders a connected group of triangles. One triangle is defined for each vertex
	presented after the first two vertices. Vertices 1, i+1, and i+2 define triangle i. 'numOfVertices/-2' triangles are rendered
	+ *AM_TRIANGLES*  renders each triplet of vertices as an independent triangle.
	Vertices 3i-2, 3i-1, and 3i define triangle i. 'numOfVertices/3' triangles are rendered
	+ *AM_POINT_LIST* 	renders each vertex as a single point. Vertex i defines point i. 'numOfVertices' points are rendered
	+ *AM_LINE_LIST* 	renders each pair of vertices as an independent line segment.
	Vertices 2i-1 and 2i define line i. 'numOfVertices/2' lines are rendered
	+ *AM_LINE_STRIP* 	renders a connected group of line segments from the first vertex to the last. Vertices i and i+1 define line i. 'numOfVertices - 1' lines are rendered

see also:
	<renderTriangle>, <vertex> struct */
	void renderVertexArray(vertex *array, unsigned int numOfVertices, int type);




	/*method: setLight
	Creates a new light numbered 'numLight' with the properties defined in  'lt'

	parameters:
	numLight - Specifies a light number. The number of lights depends on the underlying implementation,
	but at least eight lights are supported. They are identified by integers 0 .. maximum lights

	lt - a struct <light> variable which defines the properties of the 'numLight' light
	
see also:
	struct <light>, <showLight>*/
	void setLight( int numLight, const light& lt );

	/*method: showLight
	Enables/disables light 'numLight'. Light 'numLight' must first have been created with the <setLight> method.

	parameters:
	numLight - the number of the light to enable/disable

	state - *true* to enable the light, *false* otherwise
	
	see also:
		struct <light>, <setLight>*/
	void showLight( int numLight, bool state );

	/*method: renderParticle
	Renders a particle in the display

	parameters:
	x - the x coordinate of the particle position
	
	y - the y coordinate of the particle position
	
	z - the z coordinate of the particle position
	
	size - size of the particle
	
	col - colour of the particle
	
	type - can be *AM_SOFTWARE_PARTICLE* or *AM_HARDWARE_PARTICLE*

	see also:
	<renderParticleArray>*/
	void renderParticle(float x, float y, float z, float size, colour col, int type);

	/*method: renderParticleArray
	Useful for rendering lots of particles

	parameters:
	array - the array of particles

	sizeOfArray - guess what...

	type - can be *AM_SOFTWARE_PARTICLE* or *AM_HARDWARE_PARTICLE*

	see also:
	<renderParticle>*/
	void renderParticleArray(particle *array, unsigned int sizeOfArray, int type);
	void setPerspective(float fov, float ratio, float zNear, float zFar);

	/*method: project*/
	void project( float objx, float objy, float objz,
					  float *winx, float *winy, float *winz );

	/*method: unproject*/
	void unproject( float winx, float winy, float winz,
						 float *objx, float *objy, float *objz );

	/*method: planeIntersect*/
	void planeIntersect( float a, float b, float c, float d,
								float p1x, float p1y, float p1z, float p2x, float p2y, float p2z,
								float *poutx, float *pouty, float *poutz );

	/*method: enter2dMode*/
	void enter2dMode();
	/*method: leave2dMode*/
	void leave2dMode();

private:
	static bool oneGraphics;	//gia na dhmiourgeitai mono ena antikeimeno Graphics
	int width;
	int height;
	int bpp;
	bool fullScreen;
	bool vSync;
	bool lighting;
	bool alpha;
	bool culling;
	int cmode;
	bool zBuffer;
	bool cursorState;
	Texture *currentTexture;
	struct camera cam;
	bool sceneBegin; //flag gia to an egine sceneBegin
	float bgcolour[ 4 ];
	unsigned int gpuBuf;
	int parameter;
	float maxSize; //point size gia particles me point pixel
	void drawBillboardParticle(float x, float y, float z, float size, colour col);
	GraphicsImp *imp;
 	//SDL_Surface *screen;

};



class Sprite;

/*class: Texture
A class for representing 2d textures

see also:
<Graphics>*/

class Texture
{
	friend class Sprite;
private:
	colour colourKey;
	int filtering, filteringDefinition;
	bool colourKeyEnabled;
	unsigned long width;
	unsigned long height;
 // 	GLuint gl_Tex;
	Graphics *g;
	//void swap( unsigned char &a, unsigned char &b );
//	void CreateTexture( unsigned int textureArray[], char *strFileName, int textureID );
// 	int ImageLoad( const char *filename, Image *image );
	void LoadGLTextures( unsigned int *gl_Tex, const char * filename );

	friend void Graphics::setTexture( Texture *tex );

public:
	
	unsigned int gl_Tex;
	
/* group: Constructors*/
	

	/* constructor: Texture
	Creates a texture from an image file

	parameters:
		file - the filename of the image to use as a texture. Accepted formats are PNG, TGA, BMP, JPG, DDS.
		g - a pointer to a Graphics object*/
	Texture( char *file, Graphics *g);
	/* constructor: Texture
	Creates a texture from an image file

	parameters:
	file - the filename of the image to use as a texture. Accepted formats are PNG, TGA, BMP, JPG.
	key - the colour key to use
	g - a pointer to a Graphics object*/
	Texture( char *file, colour key, Graphics *g);
	/* constructor: Texture
	Creates a texture from memory

	parameters:
	data - a pointer to the textures data
	w - the width of the texture
	h - the height of the texture
	format - the format of the pixels in _data_. Accepted values are AM_RGBA, AM_ALPHA
	type - currently the one supported type is AM_BYTE
	g - a pointer to a Graphics object*/
	Texture(void *data, unsigned int w, unsigned int h, int format, int type, Graphics *g);
	~Texture();

/*group: Methods */
	void texSubImage2D(int xoff, int yoff, unsigned int w, unsigned int h, int format, int type, const void *pixels);
	void setFiltering(int f, float anisotropyLevel = 1.0);
	int getFiltering();
	/*method: getWidth
	returns:
	 the texture width */
	unsigned long getWidth();
	/*method: getHeight
	returns:
	 the texture height*/
	unsigned long getHeight();
	//unsigned int gl_getGLTex();
};


#endif
