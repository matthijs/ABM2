/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

//Include files

#include "Graphics.h"
#include "Input.h"
#include "md2model.h"
#include "Sprite.h"
#include "System.h"
#include <SDL/SDL.h>

extern bool init();
extern bool cleanup();
extern bool renderFrame();

#define _DEBUG

int key_val = 0;
bool key_press = false;
int argc = 0;
char **argv = 0;

int main( int _argc, char **_argv )
{
	argc = _argc;
	argv = _argv;


#ifndef _DEBUG
	init();
	while ( renderFrame() ) 
	{ ; }

	cleanup();
	return 0;
#else
	SDL_Event event;
	init();	
	while(renderFrame())
	{
		SDL_PollEvent(&event);
		switch( event.type )
		{
			case SDL_KEYDOWN:
				if(1 /*(&event.key.keysym)->sym == key*/)
				{  
				
					key_val = (&event.key.keysym)->sym;/* - (((&event.key.keysym)->mod & KMOD_RSHIFT) ? 32: 0);*/
					//printf("Pressed = %d\n", key_val);
					if(key_val > 256)
					{
						key_val = 0;
						break;
					}
					key_press = true;
					
				}
				break;
			case SDL_KEYUP:
				key_press = false;
				break;
			default:
				key_press = false;
				break;
		}
	}
	cleanup();
	return 0;
#endif
}

