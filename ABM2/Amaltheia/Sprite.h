/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _SPRITE
#define _SPRITE

#include "Graphics.h"
#include "Engine/mmanager.h"


/*
Class: Sprite
Represents a sprite
*/

class Sprite : public IMMObject
{
private:
	Texture *tex;
	int tileWidth;
	int tileHeight;
	Graphics *g;
	//SDL_Rect sRect;
	unsigned long texW;
	unsigned long texH;
	float texWFactor;
	float texHFactor;
	void glEnter2DMode();
	void glLeave2DMode();

public:
//group: Constructors
	
	/*constructor: Sprite

	parameters:

	filename - the filename of the image which will be our sprite
	key - the colour key to use for this sprite
	 tw - the tile width
	 th - the tile height. _tw_ and _th are useful when you have the sprite's animation in fixed tiles in an image.
	 In many cases we haven't, so we specify the full size of the image
	 g- a pointer to the Graphics object*/
	Sprite( char *filename, colour key, int tw, int th, Graphics *g);
	~Sprite(void);

//group: Methods
		
/*method: blit
	Blits a sprite on the display at point (x,y). This is the top-left point of the sprite.
	
parameters:
	x - x coordinate in screen coordinates
	y - y coordinate in screen coordinates
	scalex - the factor to scale in the X axis
	scalex - the factor to scale in the Y axis
	dx - tile index in X
	dy - tile index in Y
	colour - the colour mask to use*/
	void blit(int x, int y, float scalex, float scaley, int dx, int dy,  colour c);
	AUTO_SIZE;	
};

#endif
