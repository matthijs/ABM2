/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Input.h"
#include <SDL/SDL.h>
#include <ctype.h>


const int LEFT_BUTTON = SDL_BUTTON_LEFT;
const int MIDDLE_BUTTON =  SDL_BUTTON_MIDDLE;
const int RIGHT_BUTTON   =   SDL_BUTTON_RIGHT;

const int KEY_ESC = SDLK_ESCAPE;
const int KEY_ESCAPE = SDLK_ESCAPE;

const int KEY_MINUS     =      SDLK_MINUS;
const int KEY_EQUALS =         SDLK_EQUALS;

const int KEY_BACKSPACE =      SDLK_BACKSPACE;

const int KEY_TAB     =        SDLK_TAB;

const int KEY_LBRACKET =       SDLK_LEFTBRACKET;
const int KEY_RBRACKET   =     SDLK_RIGHTBRACKET;

const int KEY_ENTER       =    SDLK_RETURN;
const int KEY_RETURN       =   SDLK_RETURN;

const int KEY_CTRL         =   SDLK_LCTRL;
const int KEY_LCTRL         =  SDLK_LCTRL;
const int KEY_RCTRL          = SDLK_RCTRL;

const int KEY_SHIFT         =  SDLK_LSHIFT;
const int KEY_LSHIFT       =   SDLK_LSHIFT;
const int KEY_RSHIFT      =    SDLK_RSHIFT;

const int KEY_ALT         =    SDLK_LSUPER;
const int KEY_LALT        =    SDLK_LSUPER;
const int KEY_RALT     =       SDLK_RSUPER;

const int KEY_0             =  SDLK_0;
const int KEY_1            =   SDLK_1;
const int KEY_2            =   SDLK_2;
const int KEY_3            =   SDLK_3;
const int KEY_4            =   SDLK_4;
const int KEY_5            =   SDLK_5;
const int KEY_6           =    SDLK_6;
const int KEY_7          =     SDLK_7;
const int KEY_8            =   SDLK_8;
const int KEY_9              = SDLK_9;

const int KEY_A              = SDLK_a;
const int KEY_B              = SDLK_b;
const int KEY_C           =    SDLK_c;
const int KEY_D        =       SDLK_d;
const int KEY_E         =      SDLK_e;
const int KEY_F          =     SDLK_f;
const int KEY_G           =    SDLK_g;
const int KEY_H       =        SDLK_h;
const int KEY_I          =     SDLK_i;
const int KEY_J             =  SDLK_j;
const int KEY_K             =  SDLK_k;
const int KEY_L      =         SDLK_l;
const int KEY_M       =        SDLK_m;
const int KEY_N          =     SDLK_n;
const int KEY_O             =  SDLK_o;
const int KEY_P           =    SDLK_p;
const int KEY_Q      =         SDLK_q;
const int KEY_R         =      SDLK_r;
const int KEY_S            =   SDLK_s;
const int KEY_T            =   SDLK_t;
const int KEY_U             =  SDLK_u;
const int KEY_V       =        SDLK_v;
const int KEY_W        =       SDLK_w;
const int KEY_X           =    SDLK_x;
const int KEY_Y             =  SDLK_y;
const int KEY_Z              = SDLK_z;

const int KEY_SEMICOLON  =     SDLK_SEMICOLON;
const int KEY_APOSTROPHE   =   SDLK_QUOTE;

const int KEY_TILDE       =    SDLK_UNKNOWN;
const int KEY_GRAVE      =     SDLK_BACKQUOTE;

const int KEY_BACKSLASH  =     SDLK_BACKSLASH;

const int KEY_COMMA       =    SDLK_COMMA;
const int KEY_PERIOD         = SDLK_PERIOD;
const int KEY_FORWARDSLASH  =  SDLK_SLASH;
const int KEY_SLASH       =    SDLK_SLASH;

const int KEY_SPACE     =      SDLK_SPACE;

const int KEY_CAPSLOCK      =  SDLK_CAPSLOCK;
const int KEY_CAPITAL      =   SDLK_CAPSLOCK;

const int KEY_F1        =      SDLK_F1;
const int KEY_F2         =     SDLK_F2;
const int KEY_F3          =    SDLK_F3;
const int KEY_F4           =   SDLK_F4;
const int KEY_F5              = SDLK_F5;
const int KEY_F6        =      SDLK_F6;
const int KEY_F7            =  SDLK_F7;
const int KEY_F8          =    SDLK_F8;
const int KEY_F9         =     SDLK_F9;
const int KEY_F10         =    SDLK_F10;
const int KEY_F11          =   SDLK_F11;
const int KEY_F12          =   SDLK_F12;

const int KEY_SYSRQ      =     SDLK_SYSREQ;
const int KEY_SCROLLLOCK  =    SDLK_SCROLLOCK;
const int KEY_PAUSE      =     SDLK_PAUSE;

const int KEY_NUMLOCK   =      SDLK_NUMLOCK;
const int KEY_NUMPAD0     =    SDLK_KP0;
const int KEY_NUMPAD1      =   SDLK_KP1;
const int KEY_NUMPAD2     =    SDLK_KP2;
const int KEY_NUMPAD3      =   SDLK_KP3;
const int KEY_NUMPAD4      =   SDLK_KP4;
const int KEY_NUMPAD5       =  SDLK_KP5;
const int KEY_NUMPAD6      =   SDLK_KP6;
const int KEY_NUMPAD7     =    SDLK_KP7;
const int KEY_NUMPAD8      =   SDLK_KP8;
const int KEY_NUMPAD9       =  SDLK_KP9;
const int KEY_ADD        =     SDLK_PLUS;
const int KEY_SUBTRACT  =      SDLK_MINUS;
const int KEY_DIVIDE    =      SDLK_SLASH;
const int KEY_MULTIPLY   =     SDLK_ASTERISK;
const int KEY_DECIMAL     =    SDLK_PERIOD;
const int KEY_NUMPADENTER  =   SDLK_UNKNOWN;

const int KEY_INSERT    =      SDLK_INSERT;
const int KEY_DELETE     =     SDLK_DELETE;
const int KEY_HOME        =    SDLK_HOME;
const int KEY_END       =      SDLK_END;
const int KEY_PAGEUP    =      SDLK_PAGEUP;
const int KEY_PAGEDOWN  =      SDLK_PAGEDOWN;

const int KEY_UP      =        SDLK_UP;
const int KEY_DOWN   =         SDLK_DOWN;
const int KEY_LEFT       =     SDLK_LEFT;
const int KEY_RIGHT        =   SDLK_RIGHT;

const int KEY_LWIN     =       SDLK_LSUPER;
const int KEY_RWIN      =      SDLK_RSUPER;
const int KEY_APPS         =   SDLK_MENU;





Input::Input(bool keyb,bool mous)
{
    int i;
    lockBuffer = 0;

    for (i=0;i<4;i++)
        mouseLockBuffer[i]=-1;

    symbol[0] = ')';
    symbol[1] = '!';
    symbol[2] = '@';
    symbol[3] = '#';
    symbol[4] = '$';
    symbol[5] = '%';
    symbol[6] = '^';
    symbol[7] = '&';
    symbol[8] = '*';
    symbol[9] = '(';

    keyLockDuration = D;
    return ;
}

Input::~Input()
{
    return ;
}



bool Input::keyboardGetKeyState(int key)
{

    static long tm = getTime();
    bool retValue = false;

    tm = getTime();
    SDL_PumpEvents();
    SDLMod mod = SDL_GetModState();

    if(key == KEY_CTRL && (mod & KMOD_CTRL))
        retValue = true;
    else
        if(key == KEY_LCTRL && (mod & KMOD_LCTRL))
            retValue = true;
        else
            if(key == KEY_RCTRL && (mod & KMOD_RCTRL))
                retValue = true;
            else
                if(key == KEY_SHIFT	 && (mod & KMOD_SHIFT))
                    retValue = true;
                else
                    if(key == KEY_LSHIFT && (mod & KMOD_LSHIFT))
                        retValue = true;
                    else
                        if(key == KEY_RSHIFT && (mod & KMOD_RSHIFT))
                            retValue = true;
                        else
                            if(key == KEY_ALT && (mod & KMOD_ALT))
                                retValue = true;
                            else
                                if(key == KEY_LALT  && (mod & KMOD_LALT))
                                    retValue = true;
                                else
                                    if(key == KEY_RALT && (mod & KMOD_RALT))
                                        retValue = true;
                                    else
                                        if((key == KEY_CAPSLOCK || key == KEY_CAPITAL ) && (mod & KMOD_CAPS))
                                            retValue = true;
                                        else
                                            if(key == KEY_NUMLOCK && (mod & KMOD_NUM))
                                                retValue = true;
                                            else
                                            {
                                                Uint8 *keystate = SDL_GetKeyState(NULL);
                                                if( keystate[key])
                                                {
                                                    retValue = true;
                                                    key_val = 0;
                                                }
                                            }


    return retValue;
}


/*
bool Input::keyboardGetKeyState(int key)
{
	bool retValue = false;
	
	if (getTime() - lockBuffer[key] > keyLockDuration)
		lockBuffer[key]=-1;
 
// 	printf("key = %d\n", key);
	if(lockBuffer[key]==-1)
{ 
		if(key_press && key_val == key)
			retValue = true;
}
 
	return retValue;
}
*/


void Input::mousePrepare()
{
    SDL_PumpEvents();
    SDL_GetRelativeMouseState(&mx, &my);
    return;
}

long Input::mouseGetYDelta(void)
{
	return my;
}


long Input::mouseGetXDelta(void)
{
    return mx;
}


bool Input::mouseGetButtonState(int button)
{
    bool retValue = false;

    if (getTime()-mouseLockBuffer[button]>buttonLockDuration)
        mouseLockBuffer[button]=-1;

    /*
       while(SDL_PollEvent(&event))
       {
          if(event.type == SDL_MOUSEBUTTONDOWN)
          {
             if((&event.button)->button == button)
                retValue = true;
             else
             {
                if(SDL_PushEvent(&event) == -1)
                   std::cerr << "push event failed\n";
                retValue = false;
             }
             break;
          }
          else
                if(event.type == SDL_MOUSEBUTTONUP)
                   continue;
          else
                if(event.type == SDL_KEYDOWN || event.type == SDL_MOUSEMOTION)
                {
                   if(SDL_PushEvent(&event) == -1)
                      std::cerr << "push event failed\n";
                   break;
                }
           else
               break;
     
       } // while
    */
    if (mouseLockBuffer[button]==-1)
    {
        SDL_PumpEvents();
        if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(button))
            retValue = true;
    }

    return retValue;
}


void Input::lockKey(int key, int duration)
{
    if (key<0 || key>255)
        return ;
    keyLockDuration=duration;
    return ;
}

void Input::lockButton(int button, int duration)
{
    if (button<0 || button>3)
        return ;

    //if (mouseLockBuffer[button]!=-1) return ;

    mouseLockBuffer[button]=(int)(getTime());

    buttonLockDuration=duration;

    return ;
}


char Input::getChar(void)
{

    static int oldKey = 0;
    static long delay = getTime();
    static long repeat = getTime();


    if(key_val == oldKey)
    {
        repeat = getTime();
        if(getTime() - delay < D)
            return 0;
        else
            delay = getTime();
    }
    else
    {
        delay = getTime();
        if(getTime() - repeat < R)
            return 0;
        else
            repeat = getTime();
    }

    oldKey = key_val;

    if(key_press)
    {
        if(key_val == 13)
            return '\n';
        else
        {
            SDL_PumpEvents();
            SDLMod mod = SDL_GetModState();

            if((mod & KMOD_CAPS) && !(mod & KMOD_SHIFT))
                key_val = toupper(key_val);

            if((mod & KMOD_SHIFT))
            {
                if(isdigit(key_val))
                    return symbol[key_val - 48];
                if(key_val == '\'')
                    return '\"';
                if(key_val == ';')
                    return ':';
                if(key_val == ',')
                    return '<';
                if(key_val == '.')
                    return '>';
                if(key_val == '[')
                    return '{';
                if(key_val == ']')
                    return '}';
                if(key_val == '-')
                    return '_';
                if(key_val == '=')
                    return '+';
                if(key_val == '\\')
                    return '|';
                if(key_val == '/')
                    return '?';

                if(!(mod & KMOD_CAPS))
                    return toupper(key_val);
            }
            return key_val;
        }
    }

    return 0;
    /*
    	for (i=0;i<256;i++)
    		if(keyboardGetKeyState(i))
    		{
    			lockKey(i, 300);
    		
    			if(i ==13)
    				return '\n';
    		
    			return i;
    		}
    */
}

