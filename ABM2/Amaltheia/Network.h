/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _NETWORK_H
#define _NETWORK_H

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define AM_TCP	1
#define AM_UDP	2

extern char **argv;
extern int argc;

//group: Classes

/*class: Socket
A TCP/IP socket*/
class Socket
{
public:
		int s;

//group: Fields
		/*field: peer*/
		struct sockaddr_in peer;
		int type;

public:
	//group: Constructors	
	/*constructor: Socket
parameters:
	sd -  a socket descriptor	*/
	Socket(int sd);
	/*constructor: Socket
	parameters:
	type - can be *AM_TCP* or *AM_UDP*
	protocol -  0
	*/
	Socket(int type, int protocol);
	~Socket();

//group: Methods
		/*method: Bind */
		int Bind(int port);
		/*method: Listen */
		int Listen(int log);
		/*method: Accept */
		Socket * Accept(void);
		/*method: Connect */
		int Connect(void);
		/*method: Send */
		int Send(void *buffer, unsigned long size);
		/*method: Receive */
		int Receive(void *buffer, unsigned long size);
		/*method:  SendTo */
		int SendTo(void *buffer, unsigned long size);
		/*method: RecvFrom */
		int RecvFrom(void *buffer, unsigned long size);
		/*method: Select */
		int Select(unsigned long seconds, unsigned long microsecs);
		/*method: Select */
		int Select(unsigned long microsecs);
		/*method: SetPeer */
		void SetPeer(struct sockaddr_in data);
		/*method: GetPeer */
		struct sockaddr_in GetPeer(void);
		/*method: CreatePeer */
		struct sockaddr_in CreatePeer(const char *address, int port);

};


/*class: Network*/
class Network
{
	public:
		bool ok;
//group: Constructors
		/*constructor: Network */
		Network();
		~Network();
//group: Methods
		/*method: getSocket*/
		Socket * getSocket(int type, int protocol);
};


#endif
