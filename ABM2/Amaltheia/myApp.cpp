#include <amaltheia/Graphics.h>
#include <amaltheia/Input.h>
#include <amaltheia/Sprite.h>
#include <amaltheia/System.h>

Graphics *g;
Input *inp;
Texture *t;
vertex v[24];

bool init(void)
{
	g = new Graphics(1024, 768, 16, true);
	g->setBackground(COLOUR_RGBA(127, 127, 127, 255));
	g->setCullingMode( AM_CULL_NONE);
	g->createDisplay();

	t = new Texture("amaltheia.png", g);	
	inp = new Input(true, true);

	/*1st quad*/
	v[0].x = -5.0;	v[0].y = 5.0; v[0].z = -5.0;
	v[0].u = 0;	v[0].v = 0; v[0].col = COLOUR_RGBA(55, 55, 255, 255);
	v[1].x = 5.0;	v[1].y = 5.0; v[1].z = -5.0;
	v[1].u = 1;	v[1].v = 0; v[1].col = COLOUR_RGBA(50, 100, 200, 255);
	v[2].x = -5.0;	v[2].y = -5.0; v[2].z = -5.0;
	v[2].u = 0;	v[2].v = 1; v[2].col = COLOUR_RGBA(255, 255, 255, 255);
	v[3].x = 5.0;	v[3].y = -5.0; v[3].z = -5.0;
	v[3].u = 1;	v[3].v = 1; v[3].col = COLOUR_RGBA(50, 100, 200, 255);

	/*2nd quad*/
	v[4] = v[0]; v[4].z = 5.0;
	v[4].u = 1 - v[4].u;
	v[5] = v[1]; v[5].z = 5.0;
	v[5].u = 1 - v[5].u;
	v[6] = v[2]; v[6].z = 5.0;
	v[6].u = 1 - v[6].u;
	v[7] = v[3]; v[7].z = 5.0;
	v[7].u = 1 - v[7].u;
	//3rd
	v[8] = v[4];
	v[8].u = 1 - v[8].u;
	v[9] = v[0];
	v[9].u = 1 - v[9].u;
	v[10] = v[6];
	v[10].u = 1 - v[10].u;
	v[11] = v[2];
	v[11].u = 1 - v[11].u;

	//4th
	v[12] = v[1];
	v[12].u = 1 - v[12].u;
	v[13] =	v[5];
	v[13].u = 1 - v[13].u;
	v[14] =v[3];
	v[14].u = 1 - v[14].u;
	v[15] =v[7];
	v[15].u = 1 - v[15].u;

	//5th
	v[16] = v[2];
	v[17] = v[3];
	v[18] = v[6];
	v[19] = v[7];

	//6th
	v[20] = v[0];
	v[21] = v[1];
	v[22] = v[4];
	v[23] = v[5];

	g->setTexture(t);

	return true;
}


bool renderFrame()
{
	static float rx = 0.0;
	static long tm = getTime();
	
	if(inp->keyboardGetKeyState(KEY_ESC))
		return false;

	while(getTime()  - tm <= 19)
		return true;
	
	tm = getTime();

	rx += 0.5;
	
	g->beginScene();
		g->setCamera(0,0, -30, 0, 0, 0,  0, 1,0);
		g->setWorld(0,0,0, rx, 0 ,rx, 1,1,1);
		g->renderVertexArray(v, 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[4], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[8], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[12], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[16], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[20], 4, AM_TRIANGLE_STRIP);
	
		g->setWorld(10, 10, 10, 5*rx, 5*rx ,rx, 0.5, .5, .5);
		g->renderVertexArray(v, 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[4], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[8], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[12], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[16], 4, AM_TRIANGLE_STRIP);
		g->renderVertexArray(&v[20], 4, AM_TRIANGLE_STRIP);

		g->endScene();
	
	return true;
}


bool cleanup()
{
	delete inp;
	delete t;
	delete g;
	
	return true;
}
