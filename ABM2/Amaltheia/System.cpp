/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "System.h"
#include <SDL/SDL.h>


Mutex::Mutex()
{
	if(pthread_mutex_init(&mutex, NULL) != 0)
		printf("mutex init failed\n");
	
}

Mutex::~Mutex()
{
	pthread_mutex_destroy(&mutex);
}

void Mutex::lock()
{
	pthread_mutex_lock(&mutex);
}

void Mutex::unlock()
{
	pthread_mutex_unlock(&mutex);
}


unsigned long getTime(void)
{
	return SDL_GetTicks();	
}


bool createThread(void *(func)(void *), void *param)
{
	pthread_t id;
	int ret	= pthread_create(&id, NULL, func, 	param);

	if(ret == 0)
		return false;
	
	return true;
}



void setWindowCaption(char *name)
{
	SDL_WM_SetCaption(name, NULL);
}
