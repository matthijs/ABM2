/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place*  Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _INPUT
#define _INPUT

#include "System.h"
/*group: KEY codes

+ *LEFT_BUTTON* the left mouse button
+ *MIDDLE_BUTTON* the middle mouse button
+ *RIGHT_BUTTON* the right mouse button

+ *KEY_ESC* the escape key
+ *KEY_ESCAPE*

+ *KEY_MINUS*
+ *KEY_EQUALS*
+ *KEY_BACKSPACE*
+ *KEY_TAB*

+ *KEY_LBRACKET*
+ *KEY_RBRACKET*

+ *KEY_ENTER*
+ *KEY_RETURN*

+ *KEY_CTRL*
+ *KEY_LCTRL*
+ *KEY_RCTRL*

+ *KEY_SHIFT*
+ *KEY_LSHIFT*
+ *KEY_RSHIFT*

+ *KEY_ALT*
+ *KEY_LALT*
+ *KEY_RALT*

+ *KEY_0*
+ *KEY_1*
+ *KEY_2*
+ *KEY_3*
+ *KEY_4*
+ *KEY_5*
+ *KEY_6*
+ *KEY_7*
+ *KEY_8*
+ *KEY_9*

+ *KEY_A*
+ *KEY_B*
+ *KEY_C*
+ *KEY_D*
+ *KEY_E*
+ *KEY_F*
+ *KEY_G*
+ *KEY_H*
+ *KEY_I*
+ *KEY_J*
+ *KEY_K*
+ *KEY_L*
+ *KEY_M*
+ *KEY_N*
+ *KEY_O*
+ *KEY_P*
+ *KEY_Q*
+ *KEY_R*
+ *KEY_S*
+ *KEY_T*
+ *KEY_U*
+ *KEY_V*
+ *KEY_W*
+ *KEY_X*
+ *KEY_Y*
+ *KEY_Z*

+ *KEY_SEMICOLON*
+ *KEY_APOSTROPHE*

+ *KEY_TILDE*
+ *KEY_GRAVE*

+ *KEY_BACKSLASH*

+ *KEY_COMMA*
+ *KEY_PERIOD*
+ *KEY_FORWARDSLASH*
+ *KEY_SLASH*

+ *KEY_SPACE*

+ *KEY_CAPSLOCK*
+ *KEY_CAPITAL*

+ *KEY_F1*
+ *KEY_F2*
+ *KEY_F3*
+ *KEY_F4*
+ *KEY_F5*
+ *KEY_F6*
+ *KEY_F7*
+ *KEY_F8*
+ *KEY_F9*
+ *KEY_F10*
+ *KEY_F11*
+ *KEY_F12*

+ *KEY_SYSRQ*
+ *KEY_SCROLLLOCK*
+ *KEY_PAUSE*

+ *KEY_NUMLOCK*
+ *KEY_NUMPAD0*
+ *KEY_NUMPAD1*
+ *KEY_NUMPAD2*
+ *KEY_NUMPAD3*
+ *KEY_NUMPAD4*
+ *KEY_NUMPAD5*
+ *KEY_NUMPAD6*
+ *KEY_NUMPAD7*
+ *KEY_NUMPAD8*
+ *KEY_NUMPAD9*
+ *KEY_ADD*
+ *KEY_SUBTRACT*
+ *KEY_DIVIDE*
+ *KEY_MULTIPLY*
+ *KEY_DECIMAL*
+ *KEY_NUMPADENTER*

+ *KEY_INSERT*
+ *KEY_DELETE*
+ *KEY_HOME*
+ *KEY_END*
+ *KEY_PAGEUP*
+ *KEY_PAGEDOWN*

+ *KEY_UP*
+ *KEY_DOWN*
+ *KEY_LEFT*
+ *KEY_RIGHT*

+ *KEY_LWIN*
+ *KEY_RWIN*
+ *KEY_APPS*

	*/

extern const int LEFT_BUTTON;
extern const int MIDDLE_BUTTON;
extern const int RIGHT_BUTTON;

extern const int KEY_ESC;
extern const int KEY_ESCAPE;

extern const int KEY_MINUS;           
extern const int KEY_EQUALS;   
extern const int KEY_BACKSPACE;
extern const int KEY_TAB;

extern const int KEY_LBRACKET;      
extern const int KEY_RBRACKET;

extern const int KEY_ENTER;           
extern const int KEY_RETURN;         

extern const int KEY_CTRL;      
extern const int KEY_LCTRL;    
extern const int KEY_RCTRL;    

extern const int KEY_SHIFT;     
extern const int KEY_LSHIFT;   
extern const int KEY_RSHIFT;   

extern const int KEY_ALT;        
extern const int KEY_LALT;      
extern const int KEY_RALT;      

extern const int KEY_0;            
extern const int KEY_1;            
extern const int KEY_2;            
extern const int KEY_3;            
extern const int KEY_4;            
extern const int KEY_5;            
extern const int KEY_6;            
extern const int KEY_7;            
extern const int KEY_8;            
extern const int KEY_9;            

extern const int KEY_A;            
extern const int KEY_B;            
extern const int KEY_C;            
extern const int KEY_D;            
extern const int KEY_E;            
extern const int KEY_F;            
extern const int KEY_G;            
extern const int KEY_H;            
extern const int KEY_I;            
extern const int KEY_J;             
extern const int KEY_K;             
extern const int KEY_L;             
extern const int KEY_M;            
extern const int KEY_N;            
extern const int KEY_O;            
extern const int KEY_P;            
extern const int KEY_Q;            
extern const int KEY_R;            
extern const int KEY_S;            
extern const int KEY_T;            
extern const int KEY_U;            
extern const int KEY_V;            
extern const int KEY_W;           
extern const int KEY_X;           
extern const int KEY_Y;            
extern const int KEY_Z;            

extern const int KEY_SEMICOLON; 
extern const int KEY_APOSTROPHE;

extern const int KEY_TILDE;           
extern const int KEY_GRAVE;          

extern const int KEY_BACKSLASH;

extern const int KEY_COMMA;        
extern const int KEY_PERIOD;        
extern const int KEY_FORWARDSLASH;
extern const int KEY_SLASH;           

extern const int KEY_SPACE;           

extern const int KEY_CAPSLOCK;     
extern const int KEY_CAPITAL;        

extern const int KEY_F1;              
extern const int KEY_F2;              
extern const int KEY_F3;              
extern const int KEY_F4;              
extern const int KEY_F5;              
extern const int KEY_F6;              
extern const int KEY_F7;              
extern const int KEY_F8;              
extern const int KEY_F9;              
extern const int KEY_F10;            
extern const int KEY_F11;            
extern const int KEY_F12;            

extern const int KEY_SYSRQ;        
extern const int KEY_SCROLLLOCK;
extern const int KEY_PAUSE;          

extern const int KEY_NUMLOCK;     
extern const int KEY_NUMPAD0;     
extern const int KEY_NUMPAD1;     
extern const int KEY_NUMPAD2;     
extern const int KEY_NUMPAD3;     
extern const int KEY_NUMPAD4;     
extern const int KEY_NUMPAD5;     
extern const int KEY_NUMPAD6;     
extern const int KEY_NUMPAD7;     
extern const int KEY_NUMPAD8;     
extern const int KEY_NUMPAD9;     
extern const int KEY_ADD;             
extern const int KEY_SUBTRACT;    
extern const int KEY_DIVIDE;         
extern const int KEY_MULTIPLY;     
extern const int KEY_DECIMAL;      
extern const int KEY_NUMPADENTER;     

extern const int KEY_INSERT;          
extern const int KEY_DELETE;         
extern const int KEY_HOME;            
extern const int KEY_END;             
extern const int KEY_PAGEUP;        
extern const int KEY_PAGEDOWN;  

extern const int KEY_UP;              
extern const int KEY_DOWN;        
extern const int KEY_LEFT;           
extern const int KEY_RIGHT;         

extern const int KEY_LWIN;          
extern const int KEY_RWIN;          
extern const int KEY_APPS;           

#define D 200 
#define R 1 


	
extern int key_val;
extern bool key_press;

//group: Classes

/*class: Input
Handles keyboard and mouse input*/
class Input
{
protected:
	int lockBuffer;
	int mouseLockBuffer[4];
	char map[512];
	int keyLockDuration;
	int buttonLockDuration;
	char symbol[10];
   //SDL_Event event;
	int mx,my;

	
public:
	
//group: 	Constructors
	/*constructor: Input
	parameters:
	keyb -  *true* to enable the keyboard subsystem, *false* otherwise
	mous -  *true* to enable the mouse subsystem, *false* otherwise
	*/
	Input(bool keyb, bool mous);
	~Input(void);

/*group: Methods */
	
/*method: mousePrepare
	This method is the actual method that makes the reading from the hardware.
	It should be called at regular intervals. If called too often or too sparsely it will result in missing some information
	from the input. In general terms it is safe just to call it in every frame. But if the frame rate if very high it might
	result in strange behavior from the mouse. We suggest assigning a timer of X ms to the calling of the function
	and fine-tune it for better results.
	
see also:
	<mouseGetButtonState>, 	<mouseGetXDelta> and <mouseGetYDelta>*/
	void mousePrepare();

/*method: mouseGetButtonState
	Checks whether a speficic mouse button is pressed

	parameters:
	button - the mouse button to check. Valid values are

	+ *LEFT_BUTTON*
	+ *MIDDLE_BUTTON*
	+ *RIGHT_BUTTON*
	
	retuns:
	*true* if _button_ is pressed, *false* otherwise

see also:
	<mousePrepare>*/
	bool mouseGetButtonState(int button);

/*method: mouseGetXDelta
	Reports deltas for mouse movement in X axis

	returns:
	the units of the mouse movement in the X axis. A negative integer will be returned when the mouse is moved in the
	left direction, and a positive when moved in the right. Zero will be returned when there is no movement

	see also:
	<mouseGetYDelta>*/
	long mouseGetXDelta(void);

/*method: mouseGetYDelta
	Reports deltas for mouse movement in Y axis
	
	returns:
	the units of the mouse movement in the Y axis

	see also:
	<mouseGetXDelta>*/
	long mouseGetYDelta(void);

/*method: lockButton
	Locks a mouse button for _duration_ milliseconds. When you lock a button it will not report its state for _duration_ millis

	button - the desired button to lock
	duration - duration of the locking in milliseconds

	see also:
	<mouseGetButtonState>*/
	void lockButton(int button, int duration=100);

/*method: keyboardGetKeyState
	Checks whether a speficic key on the keyboard is pressed

	parameters:
			key - the key to check. See the <KEY codes>

	retuns:
			*true* if _key_ is pressed, *false* otherwise*/
	bool keyboardGetKeyState(int key);

/*method: getChar
Returns the character that has been pressed on the keyboard*/
	char getChar(void);

/*method: lockKey */
	void lockKey(int key, int duration=100);
};

#endif
