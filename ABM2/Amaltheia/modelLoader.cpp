/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "modelLoader.h"

modelLoader::modelLoader(Graphics *g)
{
	gr=g;
	m[0]=new model("tris.md2", "masked.bmp",g);
	return ;
}

modelLoader::~modelLoader(void)
{
	int i;
	for (i=0;i<Different_types;i++)
		delete m[i];

	return ;
}

c_model * modelLoader::getModel(int type)
{
	c_model * ctmp;

	ctmp=new c_model(m[type]);
	ctmp->setFrameSequence("stand");

	return ctmp;
}
