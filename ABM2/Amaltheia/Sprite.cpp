/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Sprite.h"
#include <GL/gl.h>


float z_order = 1.0;


void Sprite::glEnter2DMode()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	
	glEnable(GL_TEXTURE_2D);

	
	/* This allows alpha blending of 2D textures with the scene */
	glEnable(GL_BLEND);
 	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	glViewport(0, 0, this->g->getWidth(), this->g->getHeight());

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

    	glOrtho(0.0, (GLdouble)this->g->getWidth(), (GLdouble)this->g->getHeight(), 0.0, 0.0, -2.0);
//  	gluOrtho2D(0.0, (GLdouble)this->g->getWidth(), (GLdouble)this->g->getHeight(), 0.0);
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
//	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
   return;
}


void Sprite::glLeave2DMode()
{
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
   return;
}

Sprite::Sprite( char * filename, colour key, int tw, int th, Graphics *g)
{
	this->g = g;
	tileWidth=tw;
	tileHeight=th;
	
   	glAlphaFunc(GL_NOTEQUAL, 0);
	glEnable(GL_ALPHA_TEST);
	tex = new Texture(filename, key, g);

   	texW = tex->getWidth();
   	texH = tex->getHeight();
   	texWFactor = (tileWidth / (float) texW);
   	texHFactor = (tileHeight / (float) texH);

	return ;
}


Sprite::~Sprite(void)
{
	delete tex;
	return ;
}


void Sprite::blit(int x, int y, float scalex, float scaley, int dx, int dy, colour col)
{
	z_order-=0.0001;
 	glEnter2DMode();
// 	glColor4ub((colour & 0x00FF0000) >> 16, (colour  & 0x0000FF00) >> 8,
//                         (colour & 0x000000FF), (colour & 0xFF000000) >> 24);
	glColor4f(col.r, col.g, col.b, col.a);

	glBindTexture(GL_TEXTURE_2D, tex->gl_Tex);
 	glPushMatrix();
	glTranslatef(x,y, 0);

	glScalef(scalex, scaley, 1);
	glBegin(GL_QUADS);
		glTexCoord2f(dx*texWFactor, 1 - dy*texHFactor);
		glVertex3f(0,0, z_order);

		glTexCoord2f(dx*texWFactor,  1 - (dy + 1)*texHFactor);
		glVertex3f(0,  tileHeight, z_order);

		glTexCoord2f((dx + 1)*texWFactor,  1 - (dy + 1)*texHFactor);
		glVertex3f( tileWidth, tileHeight, z_order);

		glTexCoord2f((dx + 1)*texWFactor, 1 - dy*texHFactor);
		glVertex3f( tileWidth, 0, z_order);
	glEnd();
 	glPopMatrix();

	glLeave2DMode();


   return ;
}
