/***************************************************************************
 *   Copyright (C) 2005 by Dimitris Saougos & Filippos Papadopoulos   *
 *   <psybases@gmail.com>                                                             *
 *                                                                                                       *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                                    *
 *                                                                                                           *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _MD2MODEL
#define _MD2MODEL


// #include "Texture.h"
#include "Graphics.h"

// using namespace Amaltheia;

class model;
class c_model;

typedef unsigned char BYTE;

struct tex
{
	short u, v;
};

struct texturecoord
{
	float u;
	float v;
};

struct face
{
	short vindex[ 3 ];
	short tindex[ 3 ];
};

struct triangle
{
	BYTE vertex[ 3 ];
	BYTE lightNormalIndex;
};

struct frame
{
	float scale[ 3 ];
	float translate[ 3 ];
	char name[ 16 ];
	struct triangle **tr;
};

struct MD2header
{
	int magic;					// This is used to identify the file
	int version;					// The version number of the file (Must be 8)
	int skinWidth;				// The skin width in pixels
	int skinHeight;				// The skin height in pixels
	int frameSize;				// The size in bytes the frames are
	int numSkins;				// The number of skins associated with the model
	int numVertices;				// The number of vertices (constant for each frame)
	int numTexCoords;			// The number of texture coordinates
	int numTriangles;			// The number of faces (polygons)
	int numGlCommands;			// The number of gl commands
	int numFrames;				// The number of animation frames
	int offsetSkins;				// The offset in the file for the skin data
	int offsetTexCoords;			// The offset in the file for the texture data
	int offsetTriangles;			// The offset in the file for the face data
	int offsetFrames;			// The offset in the file for the frames data
	int offsetGlCommands;		// The offset in the file for the gl commands data
	int offsetEnd;				// The end of the file offset
};

struct fnode
{
	int fnumber;
	struct fnode *next;
};
typedef struct fnode flist;




class model
{
private:
	// 	void delframelist(void);
	int framecounter;
	char *bitmap_filename;
	Texture *modelTexture;
	bool change;
	Graphics *graph;
// 	GLfloat *v;
	float *v;
	//    bool minMax; //ginetai true otan kaleitai h setframeseq. To elegxei h getMin...

public:
	bool *mark;
	float xmin, xmax, ymin, ymax, zmin, zmax;
	struct MD2header md2h;
	struct tex *tex1;
	struct texturecoord *tc;
	struct face *fc;
	struct frame **fr;
	bool ok;
	float FPS;
	flist *cpointer;

	model( char *, char *, Graphics *g );
	~model();

	void render( float t );
	int getframecounter( void );

};




class c_model
{
private:
	model *mod;
	flist *fhead, *ftail;
	int nframes;
	void delFrameSequence( void );
	int current_offset;
	float oldt;
	float gettime( void );

public:
	float FPS;
	float xmin, xmax, ymin, ymax, zmin, zmax;
	flist *cpointer;
	char *current_frame_sequence;

	c_model( model *m );
	~c_model();
	void setFrameSequence( char *name );
	void render( void );
};

#endif
