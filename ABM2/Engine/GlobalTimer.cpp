// GlobalTimer.cpp: implementation of the CGlobalTimer class.
//
//////////////////////////////////////////////////////////////////////

#include "engine.h"
#include "GlobalTimer.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

float CGlobalTimer::dT=0;
unsigned long CGlobalTimer::lastFrameIndex=0;
unsigned long CGlobalTimer::thisFrameIndex=0;

CGlobalTimer::CGlobalTimer(CKernel *kernel) : ITask(kernel)
{

}

CGlobalTimer::~CGlobalTimer()
{

}

bool CGlobalTimer::Start()
{
	thisFrameIndex=GetTickCount();
	lastFrameIndex=thisFrameIndex;
	dT=0;
	return true;
}

void CGlobalTimer::Update()
{
	lastFrameIndex=thisFrameIndex;
	thisFrameIndex=GetTickCount();
	dT=((float)(thisFrameIndex-lastFrameIndex))/1000.0f;
}

void CGlobalTimer::Stop()
{

}
