#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#pragma warning ( disable : 4786 )

#include <iostream>
#include <ostream>
#include <istream>
#include <fstream>
#include <list>
#include <string>
#include <map>
#include <deque>
#include <vector>
#include <stack>
#include <algorithm>
#include <functional>
#include <cassert>
#include <sstream>
#include <cmath>
#include <ctime>

#include "porting.h"

//even though resource.h is technically a Win32-resource file, it's just a bunch of defines, so it's ok (and necessary)
#include "resource.h"
/*
#include <sdl.h>
#include <sdl_net.h>
#include <sdl_image.h>
#include <sdl_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <fmod.h>
*/
//foundation layer headers
#include "Log.h"
#include "mmanager.h"
#include "singleton.h"
#include "functor.h"
#include "dator.h"
#include "Kernel.h"
#include "Settings.h"
#include "profiler.h"
#include "ProfileLogHandler.h"


//task pool
#include "VideoUpdate.h"
#include "GlobalTimer.h"
#include "InputTask.h"
#include "SoundTask.h"

//misc
#include "misc.h"
#include "math.h"

/** Where should this be? */
enum SpriteID {
	SPR_NONE = 0,
	SPR_FIELD,
	SPR_TILE,
	SPR_PLAYER,
};

enum {
	SPR_SUB_BRICK = 0,
	SPR_SUB_SOLID = 1,
	SPR_SUB_CLEAR = 2,
};

#endif
