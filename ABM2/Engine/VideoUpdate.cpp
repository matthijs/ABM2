// VideoUpdate.cpp: implementation of the CVideoUpdate class.
//
//////////////////////////////////////////////////////////////////////

#include "engine.h"
#include "../main.h"
#include "VideoUpdate.h"


CMMPointer<Dator<int> > CVideoUpdate::screenWidth=0;
CMMPointer<Dator<int> > CVideoUpdate::screenHeight=0;
CMMPointer<Dator<int> > CVideoUpdate::screenBPP=0;
int CVideoUpdate::scrWidth=640;
int CVideoUpdate::scrHeight=480;
int CVideoUpdate::scrBPP=16;

CVideoUpdate::CVideoUpdate(CKernel* kernel) : ITask(kernel)
{
	assert(screenWidth && screenHeight && screenBPP);
	this->g = new Graphics(scrWidth, scrHeight, 16, false);
}

CVideoUpdate::~CVideoUpdate()
{
}

bool CVideoUpdate::Start()
{
/*
	if(-1==SDL_InitSubSystem(SDL_INIT_VIDEO))
	{
		CLog::Get().Write(LOG_CLIENT,IDS_GENERIC_SUB_INIT_FAIL,"Video",SDL_GetError());
		return false;
	}
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	int flags = SDL_OPENGL | SDL_ANYFORMAT | SDL_FULLSCREEN;

	if(!SDL_SetVideoMode(scrWidth, scrHeight, scrBPP, flags))
	{
		CLog::Get().Write(LOG_CLIENT, IDS_BAD_DISPLAYMODE, scrWidth, scrHeight, scrBPP, SDL_GetError());
		return false;
	}

	//hide the mouse cursor
	SDL_ShowCursor(SDL_DISABLE);
*/
	if (this->g->createDisplay())
		return false;
	this->g->setCullingMode(AM_CULL_NONE);
	this->g->setBackground(COLOUR_RGBA(127, 127, 127, 255));
	
	/* Start a first scene */
	this->g->beginScene();
	return true;
}

void CVideoUpdate::Update()
{
	this->g->endScene();
	this->g->beginScene();
}

void CVideoUpdate::Stop()
{
	/* End the last scene */
	this->g->endScene();
	delete this->g;
}
