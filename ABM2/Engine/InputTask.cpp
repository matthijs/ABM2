// InputTask.cpp: implementation of the CInputTask class.
//
//////////////////////////////////////////////////////////////////////

#include "InputTask.h"
#include "../main.h"

#ifdef WIN32
// include the DirectX Library files
#pragma comment (lib, "dinput.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

#endif // WIN32

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//unsigned char *CInputTask::keys=0;
//CMMPointer<CMMDynamicBlob<unsigned char> > CInputTask::oldKeys=0;
//int CInputTask::keyCount=0;
BYTE CInputTask::keys[256];
BYTE CInputTask::oldKeys[256];
int CInputTask::dX=0;
int CInputTask::dY=0;
unsigned int CInputTask::buttons=0;
unsigned int CInputTask::oldButtons=0;

CInputTask::CInputTask(CKernel* kernel) : ITask(kernel)
{
#ifdef WIN32
	din = NULL;
	dinkeyboard = NULL;
	memset(keys, 0, 256);
	memset(oldKeys, 0, 256);

	// create the DirectInput interface
	DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&din, NULL);

	assert(din != NULL);

	// create the keyboard device
	din->CreateDevice(GUID_SysKeyboard, &dinkeyboard, NULL);

	dinkeyboard->SetDataFormat(&c_dfDIKeyboard); // set the data format to keyboard format

	// set the control you will have over the keyboard
	dinkeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
#endif // WIN32
}

CInputTask::~CInputTask()
{
#ifdef WIN32
	if (dinkeyboard != NULL) 
	{
		dinkeyboard->Unacquire();
		dinkeyboard->Release();
	}
	if (din != NULL) din->Release();
#endif // WIN32
}

bool CInputTask::Start()
{
#ifdef WIN32
	dinkeyboard->Acquire();
#endif // WIN32


//	keys=SDL_GetKeyState(&keyCount);
//	oldKeys=new CMMDynamicBlob<unsigned char>(keyCount);
//	dX=dY=0;
//	SDL_PumpEvents(); SDL_PumpEvents();
	return true;
}

void CInputTask::Update()
{
#ifdef WIN32
	dinkeyboard->Acquire();
	memcpy(oldKeys, keys, 256*sizeof(BYTE));
	dinkeyboard->GetDeviceState(256, (LPVOID)keys);

#endif // WIN32
//	SDL_PumpEvents();
//	oldButtons=buttons;
//	buttons=SDL_GetRelativeMouseState(&dX,&dY);
//	memcpy((unsigned char*)(*oldKeys),keys,sizeof(unsigned char)*keyCount);
//	keys=SDL_GetKeyState(&keyCount);
}

void CInputTask::Stop()
{
	//keys=0;
	//oldKeys=0;
	memset(keys, 0, 256);
	memset(oldKeys, 0, 256);
}
