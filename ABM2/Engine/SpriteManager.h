#if !defined(SPRITE_MANAGER_H__INCLUDED)
#define SPRITE_MANAGER_H__INCLUDED

#include <vector>
#include <cassert>
#include "Engine/mmanager.h"
#include "Amaltheia/Sprite.h"
#include "Amaltheia/Graphics.h"

/**
 * Used to hold data about default sprites.
 */
struct SpriteData {
	unsigned int   id;
	char*          filename;
	int            width;
	int            height;
};


class CSpriteManager
{
public:
	CSpriteManager(Graphics* g, SpriteData *sd);
	CMMPointer<Sprite> getSprite(unsigned int id);
private:
	std::vector< CMMPointer<Sprite> > loadedSprites;
};
#endif // SPRITE_MANAGER_H__INCLUDED
