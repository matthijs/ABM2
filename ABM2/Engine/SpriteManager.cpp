#include "SpriteManager.h"



CSpriteManager::CSpriteManager(Graphics *g, SpriteData *sd)
{
	/* Load all sprites */
	while (sd->id != 0)
	{
		/* Colour given is for transparency, Sprite has no constructor
		 * without a key colour... */
		Sprite* s = new Sprite(sd->filename, COLOUR_RGBA(248, 0, 248, 255), sd->width, sd->height, g);
		if (s) {
			/* Ensure the needed spot is present */
			while (this->loadedSprites.size() <= sd->id)
				this->loadedSprites.push_back(NULL);
			this->loadedSprites[sd->id] = CMMPointer<Sprite>(s);
		}
		sd++;
	}
}

CMMPointer<Sprite> CSpriteManager::getSprite(unsigned int id)
{
	return this->loadedSprites[id];
}
