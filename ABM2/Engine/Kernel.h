// Kernel.h: interface for the CKernel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KERNEL_H__4E822B7D_1078_4F70_BC8F_3BB4F83ED0AF__INCLUDED_)
#define AFX_KERNEL_H__4E822B7D_1078_4F70_BC8F_3BB4F83ED0AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "singleton.h"
#include "SpriteManager.h"


class CClient;
class CServer;

class ITask;

class CKernel : public Singleton<CKernel>
{
public:
	CKernel();
	virtual ~CKernel();

	int Execute();

	bool AddTask(const CMMPointer<ITask> &t);
	void SuspendTask(const CMMPointer<ITask> &t);
	void ResumeTask(const CMMPointer<ITask> &t);
	void RemoveTask(const CMMPointer<ITask> &t);
	void KillAllTasks();
	
	void setSpriteManager(CSpriteManager* sm) {this->sm = sm;}
	CSpriteManager* getSpriteManager() {return this->sm;}
protected:
	std::list< CMMPointer<ITask> > taskList;
	std::list< CMMPointer<ITask> > pausedTaskList;
	CSpriteManager *sm;
};

class ITask : public IMMObject
{
public:
	/** Keeps a normal pointer to the kernel, to prevent pointer loops */
	ITask(CKernel* kernel){this->kernel = kernel, this->canKill=false;this->priority=5000;}
	virtual bool Start()=0;
	virtual void OnSuspend(){};
	virtual void Update()=0;
	virtual void OnResume(){};
	virtual void Stop()=0;



	bool canKill;
	long priority;
protected:
	/**
	 * Returns a pointer to the current kernel
	 */
	CKernel *getKernel() {return this->kernel;}
private:
	CKernel* kernel;
};

#endif // !defined(AFX_KERNEL_H__4E822B7D_1078_4F70_BC8F_3BB4F83ED0AF__INCLUDED_)
