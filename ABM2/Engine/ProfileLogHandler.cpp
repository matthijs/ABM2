// ProfileLogHandler.cpp: implementation of the CProfileLogHandler class.
//
//////////////////////////////////////////////////////////////////////

#include "engine.h"
#include "ProfileLogHandler.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CProfileLogHandler::BeginOutput(float tTime)
{
	CLog::Get().Write(LOG_APP,IDS_PROFILE_HEADER1,tTime, 60.0f/tTime);
	CLog::Get().Write(LOG_APP,IDS_PROFILE_HEADER2);
}

void CProfileLogHandler::Sample(float fMin, float fAvg, float fMax, float tAvg, int callCount, std::string name, int parentCount)
{
	//char szBuf[100];
	//for(int i=0;i<parentCount;i++){szBuf[i]=' ';}
	//szBuf[parentCount]=0;
	char namebuf[256], indentedName[256];
	char avg[16], min[16], max[16], num[16], time[16];

	sprintf_s(avg,16, "%3.1f", fAvg);
	sprintf_s(min,16, "%3.1f", fMin);
	sprintf_s(max,16, "%3.1f", fMax);
	sprintf_s(time,16,"%3.1f", tAvg);
	sprintf_s(num,16, "%3d",   callCount);

	strcpy_s( indentedName,256, name.c_str());
	for( int indent=0; indent<parentCount; ++indent )
	{
		sprintf_s(namebuf,256, " %s", indentedName);
		strcpy_s( indentedName,256, namebuf);
	}

	CLog::Get().Write(LOG_APP,IDS_PROFILE_SAMPLE,min,avg,max,time,num,indentedName);
}

void CProfileLogHandler::EndOutput()
{
	CLog::Get().Write(LOG_APP,"\n");
}
