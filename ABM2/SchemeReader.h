#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>

// TODO: reference additional headers your program requires here
enum TField { BLANK, BRICK, SOLID };
enum TPowerup { BOMB, FLAME, DISEASE, KICK, SPEED, PUNCH, GRAB, SPOOGER, GOLDFLAME, TRIGGER, JELLY, SUPER_DISEASE, RANDOM };
struct Powerup {
	int BornWith;
	bool HasOverride;
	int Override;
	bool Forbidden;
};
struct PlayerData {
	int startx;
	int starty;
	int team;
};

const int PLAYGROUND_NUMFIELD_X = 15;
const int PLAYGROUND_NUMFIELD_Y = 11;
const int BOMBERMAN_MAX_PLAYERS = 10;

class SchemeReader
{
public:
	SchemeReader(char* FileName);
	~SchemeReader();
	TField PlayField[PLAYGROUND_NUMFIELD_X*PLAYGROUND_NUMFIELD_Y];
	PlayerData Player[BOMBERMAN_MAX_PLAYERS];
	Powerup Powerups[12];
	char Name[128];
	int BrickDensity;
};

