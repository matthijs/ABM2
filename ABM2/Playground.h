#pragma once

#include "Engine/engine.h"

#define PLAYGROUND_ROWS 11
#define PLAYGROUND_COLS 15
#define PLAYGROUND_BORDER_LEFT 20
#define PLAYGROUND_BORDER_TOP 66
#define PLAYGROUND_BORDER_RIGHT 20
#define PLAYGROUND_BORDER_BOTTOM 18
#define FIELD_UNIT_WIDTH 40
#define FIELD_UNIT_HEIGHT 36
#define BOMBERMAN_BASE_SPEED 
enum TMoveDirection { N,NNE,NE,NEE,E,SEE,SE,SSE,S,SSW,SW,SWW,W,NWW,NW,NNW }; //(0-15 clockwise)

class Bomberman
{
public:
	Bomberman(int xloc, int yloc);
	~Bomberman();
	void Update();
	void move(int xloc, int yloc);
	void move_to(int xloc, int yloc);
	void move_to_direction(TMoveDirection dir);
	int getY();
	int getX();
	int getHeight();
	int getWidth();
	int getCenterX();
	int getCenterY();
	int getOffsetX();
	int getOffsetY();
	int getFieldCenterX();
	int getFieldCenterY();
private:
	int x,y;
#ifdef WIN32
	LPDIRECT3DTEXTURE9 texture;
#endif // WIN32
};

class Tile
{
public:
	Tile(const CMMPointer<Sprite> &sprite, int subsprite, int col, int row);
	~Tile();
	void Draw();
private:
	CMMPointer<Sprite> sprite;
	int col, row, subsprite;
};

class Playground : public ITask
{
public:
	Playground(CKernel *kernel);
	bool Start();
	void Update();
	void Stop();
	AUTO_SIZE;
private:

	Bomberman *bomberman;
	Tile *field[PLAYGROUND_COLS][PLAYGROUND_ROWS];

	void move_up();
	void move_down();
	void move_right();
	void move_left();

	DWORD old_counter;
};
