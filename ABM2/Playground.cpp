#include "Playground.h"

#include "Engine/engine.h"
#include "Engine/VideoUpdate.h"
#include "SchemeReader.h"

Playground::Playground(CKernel* kernel) : ITask(kernel) { }

bool Playground::Start()
{
	// init playing field
	SchemeReader *scheme = new SchemeReader("Data/schemes/BASIC.SCH");

	CMMPointer<Sprite> tile_sprite = this->getKernel()->getSpriteManager()->getSprite(SPR_TILE);
	// put in non-destructible blocks
	for (int col=0; col<PLAYGROUND_COLS; col++) 
	{
		for (int row=0; row<PLAYGROUND_ROWS; row++)
		{
			int subsprite = -1;
			switch(scheme->PlayField[row*PLAYGROUND_NUMFIELD_X+col]) {
				case BRICK:
					subsprite = SPR_SUB_BRICK;
					break;
				case SOLID:
					subsprite = SPR_SUB_SOLID;
					break;
			}
			if (subsprite != -1)
				field[col][row] = new Tile(tile_sprite, subsprite, col, row);
			else
				field[col][row] = NULL;
		}
	}
/*
	// put in desctructible blocks with chance 90%
	for (int i=0; i<PLAYGROUND_COLS; i+=2) 
		for (int j=0; j<PLAYGROUND_ROWS; ++j)
			if ((double)rand()/RAND_MAX <= 0.9) field[i][j] = new Tile(foreground, 0, i, j);
	for (int i=1; i<PLAYGROUND_COLS; i+=2) 
		for (int j=0; j<PLAYGROUND_ROWS; j+=2)
			if ((double)rand()/RAND_MAX <= 0.9) field[i][j] = new Tile(foreground, 0, i, j);
*/	
	// put in a player
	bomberman = new Bomberman(20, 66);
	if (field[0][0] != NULL)
	{
		delete field[0][0];
		field[0][0] = NULL;
	}
	if (field[1][0] != NULL)
	{
		delete field[1][0];
		field[1][0] = NULL;
	}
	if (field[0][1] != NULL)
	{
		delete field[0][1];
		field[0][1] = NULL;
	}

	old_counter = GetTickCount();
	return true;
}

void Playground::Update()
{
	CMMPointer<Sprite> s = this->getKernel()->getSpriteManager()->getSprite(SPR_FIELD);
	
	s->blit(0, 0, 1.0, 1.0, 0, 0, COLOUR_RGBA(255, 255, 255, 255));
	for (int i=0; i<PLAYGROUND_COLS; ++i) 
	{
		for (int j=0; j<PLAYGROUND_ROWS; ++j)
		{
			if (field[i][j] != NULL) field[i][j]->Draw();
		}
	}

	#ifdef WIN32
	bomberman->Update();
	if (CInputTask::keyDown(DIK_RIGHT)) this->move_right();
	if (CInputTask::keyDown(DIK_LEFT)) this->move_left();
	if (CInputTask::keyDown(DIK_UP)) this->move_up();
	if (CInputTask::keyDown(DIK_DOWN)) this->move_down();


	d3dspt->End();    // end sprite drawing

	// do fps thingie
	RECT rect = {10,10,400,36};
	char buf[105];
	DWORD new_counter = GetTickCount();
	DWORD tmp = new_counter-old_counter;
	double tmp2 = 1 / (double(tmp) / 1000.0f);
	/*	if (tmp == 0)
		_itoa_s(0, buf, 5, 10);
	else
*/
	//_gcvt_s(buf, 105, tmp2, 4);
	sprintf(buf, "%2.2f", tmp2);
	font->DrawTextA(NULL, (char *)&buf, -1, &rect, DT_LEFT, D3DCOLOR_XRGB(255,255,255));

	//draw coordinates
	RECT rect2 = {210,10,300,36};
	sprintf(buf, "(%d, %d)", bomberman->getX(), bomberman->getY());
	font->DrawTextA(NULL, (char *)&buf, -1, &rect2, DT_LEFT, D3DCOLOR_XRGB(255,255,255));

	//draw bomberman_array position
	RECT rect3 = {310,10,500,36};
	sprintf(buf, "(%d, %d) - (%d,%d)", bomberman->getFieldCenterX(), bomberman->getFieldCenterY(), bomberman->getOffsetX(), bomberman->getOffsetY());
	font->DrawTextA(NULL, (char *)&buf, -1, &rect3, DT_LEFT, D3DCOLOR_XRGB(255,255,255));
	#endif // WIN32

	old_counter = GetTickCount();
}

void Playground::Stop()
{
	for (int i=0; i<PLAYGROUND_COLS; ++i) 
		for (int j=0; j<PLAYGROUND_ROWS; ++j)
			if (field[i][j] != NULL) delete field[i][j];

	if (bomberman != NULL) delete bomberman;
}

void Playground::move_down()
{
	// first check to stay within the playground
	if ((bomberman->getY() + bomberman->getHeight()) < (PLAYGROUND_BORDER_TOP + FIELD_UNIT_HEIGHT * PLAYGROUND_ROWS))
	{ // i can go down
		int array_x = bomberman->getFieldCenterX();
		int array_left = array_x-1;
		int array_right = array_x+1;
		int array_y = bomberman->getFieldCenterY();
		int array_below = array_y+1;

		if ((array_below <= PLAYGROUND_ROWS) && (field[array_x][array_below] == NULL) &&
			(bomberman->getOffsetX() == 0))//center of field
		{
			//bomberman->move_to(array_x, array_below);
			bomberman->move_to_direction(S);
			return;
		}
		//special cases for down-left :P
		if ((field[array_x][array_below] == NULL) && (bomberman->getOffsetX() == 10) && (bomberman->getOffsetY()%2 == 1))
		{
			bomberman->move_to_direction(SW);
			return;
		}
		if ((field[array_x][array_below] == NULL) && (bomberman->getOffsetX() == 12) && (bomberman->getOffsetY() == 0))
		{
			bomberman->move_to_direction(SW);
			return;
		}

		//directly above (according to getCenterX), offset 0-18, down left
		if ((field[array_x][array_below] == NULL) &&
			(bomberman->getOffsetX() >= (FIELD_UNIT_HEIGHT/3)))
		{
			bomberman->move_to_direction(SWW);
			return;
		}
		if ((field[array_x][array_below] == NULL) &&
			(bomberman->getOffsetX() < (FIELD_UNIT_HEIGHT/3)))
		{
			bomberman->move_to_direction(SSW);
			return;
		}
		//offset 20-36 = down left
		if ((field[array_left][array_below] == NULL) && (field[array_x][array_below] != NULL) &&
			(bomberman->getOffsetX() >= (FIELD_UNIT_WIDTH/2)) && (bomberman->getOffsetX() < FIELD_UNIT_WIDTH-2))
		{
			bomberman->move_to_direction(SWW);
			return;
		}
		//offset 4-18 = down right
		if ((field[array_right][array_below] == NULL) && (field[array_x][array_below] != NULL) && 
			(bomberman->getOffsetX() < (FIELD_UNIT_WIDTH/2)) && (bomberman->getOffsetX() > 2))
		{
			bomberman->move_to_direction(SEE);
			return;
		}
		//directly above (according to getCenterX), offset 20-38, down right
		if ((field[array_x][array_below] == NULL) &&
			(bomberman->getOffsetX() >= (FIELD_UNIT_WIDTH-FIELD_UNIT_HEIGHT/3)))
		{
			bomberman->move_to_direction(SEE);
			return;
		}
		if ((field[array_x][array_below] == NULL))
		{
			bomberman->move_to_direction(SSE);
		}
	}
}

void Playground::move_up()
{
	// first check to stay within the playground
	if (bomberman->getY() > PLAYGROUND_BORDER_TOP)
	{ // i can go up
		int array_x = bomberman->getFieldCenterX();
		int array_left = array_x-1;
		int array_right = array_x+1;
		int array_y = bomberman->getFieldCenterY();
		int array_above = array_y-1;

		if ((array_above == -1) || (field[array_x][array_above] == NULL))
		{
			bomberman->move_to(array_x, array_above);
			return;
		}
		if ((field[array_left][array_above] == NULL) && (field[array_x][array_above] != NULL) && 
			(bomberman->getCenterX()-PLAYGROUND_BORDER_LEFT < (array_x*40+20)))//linkerkant van midden
		{
			bomberman->move_to(array_left, array_above);
			return;
		}
		if ((field[array_right][array_above] == NULL) && (field[array_x][array_above] != NULL) && 
			(bomberman->getCenterX()-PLAYGROUND_BORDER_LEFT > (array_x*40+20)))//rechts van midden
		{
			bomberman->move_to(array_right, array_above);
			return;
		}
	}
}

void Playground::move_left()
{
	// first check to stay within the playground
	if (bomberman->getX() > PLAYGROUND_BORDER_LEFT)
	{ // i can go left
		int array_x = bomberman->getFieldCenterX();
		int array_left = array_x-1;
		int array_y = bomberman->getFieldCenterY();
		int array_below = array_y+1;
		int array_above = array_y-1;

		if ((array_left == -1) || (field[array_left][array_y] == NULL))
		{
			bomberman->move_to(array_left, array_y);
			return;
		}
		if ((field[array_left][array_above] == NULL) && (field[array_left][array_y] != NULL) && 
			(bomberman->getCenterY()-PLAYGROUND_BORDER_TOP < (array_y*36+18)))
		{
			bomberman->move_to(array_left, array_above);
			return;
		}
		if ((field[array_left][array_below] == NULL) && (field[array_left][array_y] != NULL) && 
			(bomberman->getCenterY()-PLAYGROUND_BORDER_TOP > (array_y*36+18)))
		{
			bomberman->move_to(array_left, array_below);
			return;
		}
	}
}

void Playground::move_right()
{
	// first check to stay within the playground
	if ((bomberman->getX() + bomberman->getWidth()) < (20 + 40 * PLAYGROUND_COLS))
	{ // i can go right
		int array_x = bomberman->getFieldCenterX();
		int array_right = array_x+1;
		int array_y = bomberman->getFieldCenterY();
		int array_below = array_y+1;
		int array_above = array_y-1;

		if ((array_right < PLAYGROUND_COLS) && (field[array_right][array_y] == NULL))
		{
			bomberman->move_to(array_right, array_y);
			return;
		}
		if ((field[array_right][array_above] == NULL) && (field[array_right][array_y] != NULL) && (bomberman->getCenterY()-PLAYGROUND_BORDER_TOP < (array_y*36+18)))
		{
			bomberman->move_to(array_right, array_above);
			return;
		}
		if ((field[array_right][array_below] == NULL) && (field[array_right][array_y] != NULL) && (bomberman->getCenterY()-PLAYGROUND_BORDER_TOP > (array_y*36+18)))
		{
			bomberman->move_to(array_right, array_below);
			return;
		}
	}
}

// =============================================
// Bomberman class
// =============================================

Bomberman::Bomberman(int xloc, int yloc)
{
	#ifdef WIN32
	HRESULT res = D3DXCreateTextureFromFile(d3ddev, L"data/powkick.png", &texture);
	if (res != D3D_OK) texture = NULL;
	#endif // WIN32
	x = xloc;
	y = yloc;
}

Bomberman::~Bomberman()
{
	#ifdef WIN32
	if (texture != NULL) texture->Release();
	#endif // WIN32
}

void Bomberman::Update()
{
	#ifdef WIN32
	D3DXVECTOR3 center(0.0f, 0.0f, 0.0f);    // center at the upper-left corner
	D3DXVECTOR3 position((FLOAT)x, (FLOAT)y, 0.0f);    // position at 50, 50 with no depth
	RECT rect = {0,0,40,36};
	d3dspt->Draw(texture, &rect, &center, &position, D3DCOLOR_XRGB(255,255,255));
	#endif // WIN32
}

void Bomberman::move(int xloc, int yloc)
{
	x += xloc;
	y += yloc;
}

int Bomberman::getY()
{
	return y;
}

int Bomberman::getX()
{
	return x;
}

int Bomberman::getHeight()
{
	return 36;
}

int Bomberman::getWidth()
{
	return 40;
}

int Bomberman::getCenterX()
{
	return (this->getX()+(FIELD_UNIT_WIDTH/2));
}

int Bomberman::getCenterY()
{
	return (this->getY()+(FIELD_UNIT_HEIGHT/2));
}

int Bomberman::getOffsetX()
{
	return (this->getX()- PLAYGROUND_BORDER_LEFT) % FIELD_UNIT_WIDTH;
}

int Bomberman::getOffsetY()
{
	return (this->getY()- PLAYGROUND_BORDER_TOP) % FIELD_UNIT_HEIGHT;
}

int Bomberman::getFieldCenterX()
{
	return (this->getCenterX() - PLAYGROUND_BORDER_LEFT)/FIELD_UNIT_WIDTH;
}

int Bomberman::getFieldCenterY()
{
	return (this->getCenterY() - PLAYGROUND_BORDER_TOP)/FIELD_UNIT_HEIGHT;
}

void Bomberman::move_to(int xloc, int yloc)
{
	/*
	float center_x = (xloc-1)*40+20+20;
	float center_y = (yloc-1)*36+66+18;
	float dist_x = center_x - this->x;
	float dist_y = center_y - this->y;

	double dist = sqrt(dist_x*dist_x + dist_y*dist_y);
	*/
	int x_move = (xloc*FIELD_UNIT_WIDTH+PLAYGROUND_BORDER_LEFT) - x;
	int y_move = (yloc*FIELD_UNIT_HEIGHT+PLAYGROUND_BORDER_TOP) - y;

	if ((x_move == 0) && (y_move > 0)) this->move(0,2);
	if ((x_move == 0) && (y_move < 0)) this->move(0,-2);
	if ((y_move == 0) && (x_move > 0)) this->move(2,0);
	if ((y_move == 0) && (x_move < 0)) this->move(-2,0);
	if ((x_move < 0) && (y_move < 0)) this->move(-1,-1);
	if ((x_move < 0) && (y_move > 0)) this->move(-1,1);
	if ((x_move > 0) && (y_move < 0)) this->move(1,-1);
	if ((x_move > 0) && (y_move > 0)) this->move(1,1);
}

void Bomberman::move_to_direction(TMoveDirection dir)
{
	switch(dir)
	{
		case N: this->move(0,-2); break;
		case NNE: this->move(1,-2); break;
		case NE: this->move(1,-1); break;
		case NEE: this->move(2,-1); break;
		case E: this->move(2,0); break;
		case SEE: this->move(2,1); break;
		case SE: this->move(1,1); break;
		case SSE: this->move(1,2); break;
		case S: this->move(0,2); break;
		case SSW: this->move(-1,2); break;
		case SW: this->move(-1,1); break;
		case SWW: this->move(-2,1); break;
		case W: this->move(-2,0); break;
		case NWW: this->move(-2,-1); break;
		case NW: this->move(-1,-1); break;
		case NNW: this->move(-1,-2); break;
	}
}

// =============================================
// Tile class
// =============================================

Tile::Tile(const CMMPointer<Sprite> &sprite, int subsprite, int col, int row)
{
	this->sprite = sprite;
	this->col = col;
	this->row = row;
	this->subsprite = subsprite;
}

Tile::~Tile()
{
}
void Tile::Draw()
{
	this->sprite->blit(
		PLAYGROUND_BORDER_LEFT + this->col * FIELD_UNIT_WIDTH,
		PLAYGROUND_BORDER_TOP + this->row * FIELD_UNIT_HEIGHT,
		1.0, 1.0, this->subsprite, 0, COLOUR_RGBA(255, 255, 255, 255)
	);
}

