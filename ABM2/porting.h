#if !defined(AFX_PORTING_H__90C3FE42_3400_4978_8BC5_7D905F3C8E63__INCLUDED_)
#define AFX_PORTING_H__90C3FE42_3400_4978_8BC5_7D905F3C8E63__INCLUDED_

#ifdef WIN32
	#include <windows.h>
#else
	#define vsprintf_s vsnprintf
	#define sprintf_s snprintf
	#define strcpy_s(dst, size, src) strncpy(dst, src, size)
	#define ZeroMemory(buf, size) memset(buf, 0, size)

	#define BYTE unsigned char
	#define DWORD unsigned int
	#define max(a,b) (a>b?a:b)
	#define min(a,b) (a<b?a:b)
#endif // !WIN32

#ifdef WITH_SDL
	#include <SDL/SDL.h>
	#define GetTickCount SDL_GetTicks
	#define Sleep SDL_Delay
#endif // WITH_SDL

#endif // !defined(AFX_PORTING_H__90C3FE42_3400_4978_8BC5_7D905F3C8E63__INCLUDED_)
